﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
    public class DirectorOfFilmsServices : DirectorOfFilmInterface
    {
        private readonly FilmsDBContext _context;
        public DirectorOfFilmsServices(FilmsDBContext context)
        {
            _context = context;
        }



        public IEnumerable<DirectorOfFilm> AddDirectorOfFilm(List<DirectorOfFilm> directorOfFilm)
        {
            try
            {
                if (directorOfFilm != null)
                {
                    foreach (DirectorOfFilm a in directorOfFilm)
                    {

                        _context.DirectorOfFilms.Add(a);//add new DirectorOfFilm
                        _context.SaveChanges();//save all changes
                    }
                }
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Lỗi");
            }
            return directorOfFilm;//return DirectorOfFilm
        }

        public DirectorOfFilm DeleteDirectorOfFilm(int idFilm)
        {
            try
            {
                var actor = GetAllDirectorOfFilmByIdFilm(idFilm);
                if (actor != null)
                {
                    foreach (DirectorOfFilm a in actor)
                    {
                        _context.DirectorOfFilms.Remove(a);//delete DirectorOfFilm
                        _context.SaveChanges();//save all changes
                    }
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex);
            }
            return null;
        }

        public DirectorOfFilm EditDirectorOfFilm(DirectorOfFilm DirectorOfFilm)
        {
            try
            {
                _context.Entry(DirectorOfFilm).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return DirectorOfFilm;
        }

        public IEnumerable<DirectorOfFilm> GetAllDirectorOfFilm()
        {
            return _context.DirectorOfFilms.ToList(); //return DirectorOfFilm list 
        }

        public IEnumerable<DirectorOfFilm> GetAllDirectorOfFilmByIdFilm(int idFilm)
        {
            var DirectorOfFilm = _context.DirectorOfFilms.Where(s => s.idFilm == idFilm).ToList();//find DirectorOfFilm with the id
            return DirectorOfFilm; //return DirectorOfFilm
        }

       
    }
}
