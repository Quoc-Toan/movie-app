﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Category_BUS.Service
{
   public class CategorysService: CategoriesInterface
    {
        private readonly FilmsDBContext _context;
        public CategorysService(FilmsDBContext context)
        {
            _context = context;
        }

        public Category AddCategory(Category Category)
        {
            try
            {
                _context.Categories.Add(Category);//add new Category
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Category;//return Category
        }

        public Category DeleteCategory(Category Category)
        {
            try
            {
                _context.Categories.Remove(Category);//delete Category
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Category;
        }

        public Category EditCategory(Category Category)
        {
            try
            {
                _context.Entry(Category).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Category;
        }

        public IEnumerable<Category> GetAllCategory()
        {
            return _context.Categories.ToList(); //return Category list 
        }

        public Category GetAllCategoryById(int id)
        {
            var Category = _context.Categories.Find(id);//find Category with the id
            return Category; //return Category
        }

        public IEnumerable<Category> Search(string CategoryName)
        {
            var Category = _context.Categories.Where(s => s.categoryName.Contains(CategoryName)).ToList();   // get data CategoryName from Controller

            return Category;
        }

        public bool CategoryIdentity(Category value)
        {
            var acc = _context.Categories.ToList();
            foreach (Category a in acc)
            {
                if (a.categoryName == value.categoryName)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
