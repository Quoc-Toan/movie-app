﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rating_BUS.Service
{
   public class RatingsService: RatingInterface
    {
        private readonly FilmsDBContext _context;
        private FilmsInterface _film;
        public RatingsService(FilmsDBContext context, FilmsInterface film)
        {
            _context = context;
            _film = film;
        }

        public Rating AddRating(Rating rating)
        {
            try
            {
                _context.Ratings.Add(rating);//add new Rating
                _context.SaveChanges();//save all changes

                UpdateRate(rating.idFilm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rating;//return Rating
        }

        public Rating DeleteRating(Rating rating)
        {
            try
            {
                _context.Ratings.Remove(rating);//delete Rating
                _context.SaveChanges();//save all changes

                UpdateRate(rating.idFilm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rating;
        }

        public Rating EditRating(Rating rating)
        {
            try
            {
                _context.Entry(rating).State = EntityState.Modified;
                _context.SaveChanges();

                UpdateRate(rating.idFilm);
               

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rating;
        }

        public IEnumerable<Rating> GetAllRating()
        {
            return _context.Ratings.ToList(); //return Rating list 
        }

        public Rating GetAllRatingById(int id)
        {
            var Rating = _context.Ratings.Find(id);//find Rating with the id
            return Rating; //return Rating
        }

        private void UpdateRate(int idFilm)
        {
            double resuilt = 0;
            int size = 0;
            try
            {
                var rating = _context.Ratings.Where(s => s.idFilm == idFilm).ToList();   // get data Rating film
                if (rating != null)
                {
                    foreach (Rating r in rating)
                    {
                        resuilt = resuilt + r.rating;
                        size++;
                    }
                }

                var a = _film.GetAllFilmById(idFilm);
                a.rate = Math.Round(resuilt/size, 1);
                _film.EditFilm(a);
            } catch { }
             
        }

        public IEnumerable<Rating> SearchByIdAccount (int idAccount)
        {
            var Rating = _context.Ratings.Where(s => s.idAccount == idAccount).ToList();   // get data RatingName from Controller

            return Rating;
        }

      
    }
}
