﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
    public class ActorOfFilmsServices : ActorOfFilmInterface
    {
        private readonly FilmsDBContext _context;
        public ActorOfFilmsServices(FilmsDBContext context)
        {
            _context = context;
        }



        public IEnumerable<ActorOfFilm> AddActorOfFilm(List<ActorOfFilm> actorOfFilm)
        {
            try
            {
                if (actorOfFilm != null)
                {
                    foreach (ActorOfFilm a in actorOfFilm)
                    {

                        _context.ActorOfFilms.Add(a);//add new ActorOfFilm
                        _context.SaveChanges();//save all changes
                    }
                }
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Lỗi");
            }
            return actorOfFilm;//return ActorOfFilm
        }

        public ActorOfFilm DeleteActorOfFilm(int idFilm)
        {
            try
            {
                var actor = GetAllActorOfFilmByIdFilm(idFilm);
                if (actor != null)
                {
                    foreach (ActorOfFilm a in actor)
                    {
                        _context.ActorOfFilms.Remove(a);//delete ActorOfFilm
                        _context.SaveChanges();//save all changes
                    }
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex);
            }
            return null;
        }

        public ActorOfFilm EditActorOfFilm(ActorOfFilm ActorOfFilm)
        {
            try
            {
                _context.Entry(ActorOfFilm).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return ActorOfFilm;
        }

        public IEnumerable<ActorOfFilm> GetAllActorOfFilm()
        {
            return _context.ActorOfFilms.ToList(); //return ActorOfFilm list 
        }

        public IEnumerable<ActorOfFilm> GetAllActorOfFilmByIdFilm(int idFilm)
        {
            var ActorOfFilm = _context.ActorOfFilms.Where(s => s.idFilm == idFilm).ToList();//find ActorOfFilm with the id
            return ActorOfFilm; //return ActorOfFilm
        }

       
    }
}
