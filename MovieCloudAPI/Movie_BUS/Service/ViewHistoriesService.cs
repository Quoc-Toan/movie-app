﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
    public class ViewHistoriesServices : ViewHistoryInterface
    {
        private readonly FilmsDBContext _context;
        public ViewHistoriesServices(FilmsDBContext context)
        {
            _context = context;
        }

        public ViewHistory AddViewHistory(ViewHistory viewHistory)
        {
            try
            {
                _context.ViewHistories.Add(viewHistory);//add new viewHistory
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return viewHistory;//return viewHistory
        }

        public ViewHistory DeleteViewHistory(ViewHistory viewHistory)
        {
            try
            {
                _context.ViewHistories.Remove(viewHistory);//delete viewHistory
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return viewHistory;
        }

        public ViewHistory EditViewHistory(ViewHistory viewHistory)
        {
            try
            {
                _context.Entry(viewHistory).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return viewHistory;
        }

        public IEnumerable<ViewHistory> GetAllViewHistory()
        {
            return _context.ViewHistories.ToList(); //return viewHistory list 
        }

        public ViewHistory GetAllViewHistoryById(int id)
        {
            var viewHistory = _context.ViewHistories.Find(id);//find viewHistory with the id
            return viewHistory; //return viewHistory
        }

        public IEnumerable<ViewHistory> GetByIdAccount(int id)
        {
            var viewHistory = _context.ViewHistories.Where(s => s.idAccount == id).ToList();   // get data viewHistoryName from Controller

            return viewHistory;
        }



        /*   public IEnumerable<viewHistory> Search(string viewHistoryName)
           {
              // var viewHistory = _context.viewHistorys.Where(s => s.Contains(viewHistoryName)).ToList();   // get data viewHistoryName from Controller

               return viewHistory;
           }*/
    }
}
