﻿using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using Movie_DAO.Models.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
    public class IdentifyServices : IdentifyInterface
    {
        private readonly FilmsDBContext _context;
        public IdentifyServices(FilmsDBContext context)
        {
            _context = context;
        }

        public bool AccountExists(string username)
        {
            var acc = _context.Accounts.FirstOrDefault(x => x.username == username);
            if (acc != null)
            {
                return true;
            }
            return false;
        }

        public bool AccountIdentity(LoginAccount value)
        {
            var acc = _context.Accounts.FirstOrDefault(x => x.username == value.username && x.password == value.password && x.role == 1 &&x.status == true);
            if (acc != null)
            {
                return true;
            }
            return false;
        }

        public Account AddProduct(Account account)
        {
            try
            {
                _context.Accounts.Add(account);//add new product
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return account;//return product
        }
    }
}
