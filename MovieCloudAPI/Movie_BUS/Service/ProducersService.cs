﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer_BUS.Service
{
   public class ProducersServices: ProducersInterface
    {
        private readonly FilmsDBContext _context;
        public ProducersServices(FilmsDBContext context)
        {
            _context = context;
        }

        public Producer AddProducer(Producer producer)
        {
            try
            {
                _context.Producers.Add(producer);//add new Producer
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return producer;//return Producer
        }

        public Producer DeleteProducer(Producer producer)
        {
            try
            {
                _context.Producers.Remove(producer);//delete Producer
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return producer;
        }

        public Producer EditProducer(Producer producer)
        {
            try
            {
                _context.Entry(producer).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return producer;
        }

        public IEnumerable<Producer> GetAllProducer()
        {
            return _context.Producers.ToList(); //return Producer list 
        }

        public Producer GetAllProducerById(int id)
        {
            var Producer = _context.Producers.Find(id);//find Producer with the id
            return Producer; //return Producer
        }

        public IEnumerable<Producer> Search(string producerName)
        {
            var Producer = _context.Producers.Where(s => s.producerName.Contains(producerName)).ToList();   // get data ProducerName from Controller

            return Producer;
        }

        public bool ProducerIdentity(Producer value)
        {
            var acc = _context.Producers.ToList();
            foreach (Producer a in acc)
            {
                if (a.producerName == value.producerName)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
