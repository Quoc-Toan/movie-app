﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Director_BUS.Service
{
   public class DirectorsServices: DirectorsInterface
    {
        private readonly FilmsDBContext _context;
        public DirectorsServices(FilmsDBContext context)
        {
            _context = context;
        }

        public Director AddDirector(Director director)
        {
            try
            {
                _context.Directors.Add(director);//add new Director
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return director;//return Director
        }

        public Director DeleteDirector(Director director)
        {
            try
            {
                _context.Directors.Remove(director);//delete Director
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return director;
        }

        public Director EditDirector(Director director)
        {
            try
            {
                _context.Entry(director).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return director;
        }

        public IEnumerable<Director> GetAllDirector()
        {
            return _context.Directors.ToList(); //return Director list 
        }

        public Director GetAllDirectorById(int id)
        {
            var Director = _context.Directors.Find(id);//find Director with the id
            return Director; //return Director
        }

        public IEnumerable<Director> Search(string directorName)
        {
            var Director = _context.Directors.Where(s => s.directorName.Contains(directorName)).ToList();   // get data DirectorName from Controller

            return Director;
        }

        public bool DirectorIdentity(Director value)
        {
            var acc = _context.Directors.ToList();
            foreach (Director a in acc)
            {
                if (a.directorName == value.directorName)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
