﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comment_BUS.Service
{
   public class CommentsService: CommentInterface
    {
        private readonly FilmsDBContext _context;
        public CommentsService(FilmsDBContext context)
        {
            _context = context;
        }

        public Comment AddComment(Comment Comment)
        {
            try
            {
                _context.Comments.Add(Comment);//add new Comment
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Comment;//return Comment
        }

        public Comment DeleteComment(Comment Comment)
        {
            try
            {
                _context.Comments.Remove(Comment);//delete Comment
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Comment;
        }

        public Comment EditComment(Comment Comment)
        {
            try
            {
                _context.Entry(Comment).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Comment;
        }

        public IEnumerable<Comment> GetAllComment()
        {
            return _context.Comments.ToList(); //return Comment list 
        }

        public Comment GetAllCommentById(int id)
        {
            var Comment = _context.Comments.Find(id);//find Comment with the id
            return Comment; //return Comment
        }

        public IEnumerable<Comment> SearchByIdAccount (int idAccount)
        {
            var Comment = _context.Comments.Where(s => s.idAccount == idAccount).ToList();   // get data CommentName from Controller

            return Comment;
        }

      
    }
}
