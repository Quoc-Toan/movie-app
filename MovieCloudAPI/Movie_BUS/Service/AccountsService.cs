﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
    public class AccountsServices : AccountInterface
    {
        private readonly FilmsDBContext _context;
        public AccountsServices(FilmsDBContext context)
        {
            _context = context;
        }

        public bool AccountIdentity(Account value)
        {
           var acc =  _context.Accounts.ToList();
            foreach(Account a in acc){
                if (a.username == value.username)
                {
                    return true;
                }
               
            }
            return false;
        }

        public Account AddAccount(Account account)
        {
            try
            {
                _context.Accounts.Add(account);//add new account
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return account;//return account
        }

        public Account DeleteAccount(Account account)
        {
            try
            {
                _context.Accounts.Remove(account);//delete account
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return account;
        }

        public Account EditAccount(Account account)
        {
            try
            {
                _context.Entry(account).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return account;
        }

        public IEnumerable<Account> GetAllAccount()
        {
            return _context.Accounts.ToList(); //return account list 
        }

        public Account GetAllAccountById(int id)
        {
            var account = _context.Accounts.Find(id);//find account with the id
            return account; //return account
        }

        public IEnumerable<Account> Search(string accountName)
        {
            var account = _context.Accounts.Where(s => s.username.Contains(accountName)).ToList();   // get data accountName from Controller

            return account;
        }
    }
}
