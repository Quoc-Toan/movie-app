﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Service
{
   public class MoviesServices: FilmsInterface
    {
        private readonly FilmsDBContext _context;
        public MoviesServices(FilmsDBContext context)
        {
            _context = context;
        }

        public Film AddFilm(Film film)
        {
            Film f = new Film();
            try
            {
                _context.Films.Add(film);//add new Film
                _context.SaveChanges();//save all changes

               f =  _context.Films.Where(s => s.filmName == film.filmName).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return f;//return Film
        }

        public Film DeleteFilm(Film film)
        {
            try
            {
                _context.Films.Remove(film);//delete Film
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return film;
        }

        public Film EditFilm(Film film)
        {
            try
            {
                _context.Entry(film).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return film;
        }

        public IEnumerable<Film> GetAllFilm()
        {
            return _context.Films.ToList(); //return Film list 
        }

        public Film GetAllFilmById(int id)
        {
            var Film = _context.Films.Find(id);//find Film with the id
            return Film; //return Film
        }

        public IEnumerable<Film> Search(string filmName)
        {
            var Film = _context.Films.Where(s => s.filmName.Contains(filmName)).ToList();   // get data FilmName from Controller

            return Film;
        }

        public bool FilmIdentity(Film value)
        {
            var acc = _context.Films.ToList();
            foreach (Film a in acc)
            {
                if (a.filmName == value.filmName)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
