﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Country_BUS.Service
{
   public class CountriesService: CountriesInterface
    {
        private readonly FilmsDBContext _context;
        public CountriesService(FilmsDBContext context)
        {
            _context = context;
        }

        public Country AddCountry(Country country)
        {
            try
            {
                _context.Countries.Add(country);//add new Country
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return country;//return Country
        }

        public Country DeleteCountry(Country country)
        {
            try
            {
                _context.Countries.Remove(country);//delete Country
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return country;
        }

        public Country EditCountry(Country country)
        {
            try
            {
                _context.Entry(country).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return country;
        }

        public IEnumerable<Country> GetAllCountry()
        {
            return _context.Countries.ToList(); //return Country list 
        }

        public Country GetAllCountryById(int id)
        {
            var Country = _context.Countries.Find(id);//find Country with the id
            return Country; //return Country
        }

        public IEnumerable<Country> Search(string countryName)
        {
            var Country = _context.Countries.Where(s => s.countryName.Contains(countryName)).ToList();   // get data CountryName from Controller

            return Country;
        }

        public bool CountryIdentity(Country value)
        {
            var acc = _context.Countries.ToList();
            foreach (Country a in acc)
            {
                if (a.countryName == value.countryName)
                {
                    return true;
                }

            }
            return false;
        }

      
    }
}
