﻿using Microsoft.EntityFrameworkCore;
using Movie_BUS.Interfaces;
using Movie_DAO.DBContext;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actor_BUS.Service
{
   public class ActorsServices: ActorsInterface
    {
        private readonly FilmsDBContext _context;
        public ActorsServices(FilmsDBContext context)
        {
            _context = context;
        }

        public Actor AddActor(Actor actor)
        {
            try
            {
                _context.Actors.Add(actor);//add new Actors
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return actor;//return Actors
        }

        public Actor DeleteActor(Actor actor)
        {
            try
            {
                _context.Actors.Remove(actor);//delete Actors
                _context.SaveChanges();//save all changes
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return actor;
        }

        public Actor EditActor(Actor Actor)
        {
            try
            {
                _context.Entry(Actor).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return Actor;
        }

        public IEnumerable<Actor> GetAllActor()
        {
            return _context.Actors.ToList(); //return Actors list 
        }

        public Actor GetAllActorById(int id)
        {
            var Actor = _context.Actors.Find(id);//find Actors with the id
            return Actor; //return Actors
        }

        public IEnumerable<Actor> Search(string ActorName)
        {
            var Actor = _context.Actors.Where(s => s.actorName.Contains(ActorName)).ToList();   // get data ActorName from Controller

            return Actor;
        }

        public bool ActorIdentity(Actor value)
        {
            var acc = _context.Actors.ToList();
            foreach (Actor a in acc)
            {
                if (a.actorName == value.actorName)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
