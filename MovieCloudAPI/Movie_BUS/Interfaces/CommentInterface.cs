﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface CommentInterface
    {
        //Method to get Comment by ID 
        Comment GetAllCommentById(int id);

        //Method to get all Comments
        IEnumerable<Comment> GetAllComment();

        //Method to add new Comment
        Comment AddComment(Comment Comment);

        //Method to edit Comment information
        Comment EditComment(Comment Comment);

        //Method to delete Comment
        Comment DeleteComment(Comment Comment);

        //Method search Prodcut by CommentName
        IEnumerable<Comment> SearchByIdAccount(int idAccount);

      

    }
}
