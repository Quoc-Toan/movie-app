﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface ActorsInterface
    {
        //Method to get Actors by ID 
        Actor GetAllActorById(int id);

        //Method to get all Actors
        IEnumerable<Actor> GetAllActor();

        //Method to add new Actors
        Actor AddActor(Actor actor);

        //Method to edit Actors information
        Actor EditActor(Actor actor);

        //Method to delete Actors
        Actor DeleteActor(Actor actor);

        //Method search Prodcut by ActorName
        IEnumerable<Actor> Search(string actorName);

        //Method for Filter Actors


        bool ActorIdentity(Actor value);
    }
}
