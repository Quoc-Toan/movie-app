﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface ViewHistoryInterface
    {
        //Method to get ViewHistory by ID 
        ViewHistory GetAllViewHistoryById(int id);

        //Method to get all ViewHistorys
        IEnumerable<ViewHistory> GetAllViewHistory();

        //Method to add new ViewHistory
        ViewHistory AddViewHistory(ViewHistory viewHistory);

        //Method to edit ViewHistory information
        ViewHistory EditViewHistory(ViewHistory viewHistory);

        //Method to delete ViewHistory
        ViewHistory DeleteViewHistory(ViewHistory viewHistory);

        //Method search Prodcut by ViewHistoryName
        IEnumerable<ViewHistory> GetByIdAccount(int id);

        //Method for Filter ViewHistory
      

      
    }
}
