﻿using Movie_DAO.Model;
using Movie_DAO.Models.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
    public interface IdentifyInterface
    {
        Boolean AccountIdentity(LoginAccount login);
        Account AddProduct(Account account);
        Boolean AccountExists(String username);
    }
}
