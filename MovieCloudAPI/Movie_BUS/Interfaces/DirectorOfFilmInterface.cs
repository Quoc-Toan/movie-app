﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface DirectorOfFilmInterface
    {
        //Method to get DirectorOfFilm by ID 
        IEnumerable<DirectorOfFilm> GetAllDirectorOfFilmByIdFilm(int idFilm);

        //Method to get all DirectorOfFilms
        IEnumerable<DirectorOfFilm> GetAllDirectorOfFilm();

        //Method to add new DirectorOfFilm
        IEnumerable<DirectorOfFilm> AddDirectorOfFilm(List<DirectorOfFilm> DirectorOfFilm);

        //Method to edit DirectorOfFilm information
        DirectorOfFilm EditDirectorOfFilm(DirectorOfFilm DirectorOfFilm);

        //Method to delete DirectorOfFilm
        DirectorOfFilm DeleteDirectorOfFilm(int idFilm);

        //Method search DirectorOfFilm by DirectorOfFilmName
       // IEnumerable<DirectorOfFilm> Search(int idFilm);

       


    }
}
