﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface DirectorsInterface
    {
        //Method to get Director by ID 
        Director GetAllDirectorById(int id);

        //Method to get all Directors
        IEnumerable<Director> GetAllDirector();

        //Method to add new Director
        Director AddDirector(Director director);

        //Method to edit Director information
        Director EditDirector(Director director);

        //Method to delete Director
        Director DeleteDirector(Director director);

        //Method search Prodcut by DirectorName
        IEnumerable<Director> Search(string directorName);

        //Method for Filter Director

        bool DirectorIdentity(Director value);

    }
}
