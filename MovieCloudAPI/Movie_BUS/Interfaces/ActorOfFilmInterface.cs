﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface ActorOfFilmInterface
    {
        //Method to get ActorOfFilm by ID 
        IEnumerable<ActorOfFilm> GetAllActorOfFilmByIdFilm(int idFilm);

        //Method to get all ActorOfFilms
        IEnumerable<ActorOfFilm> GetAllActorOfFilm();

        //Method to add new ActorOfFilm 
        IEnumerable<ActorOfFilm> AddActorOfFilm(List<ActorOfFilm> actorOfFilm);

        //Method to edit ActorOfFilm information
        ActorOfFilm EditActorOfFilm(ActorOfFilm ActorOfFilm);

        //Method to delete ActorOfFilm
        ActorOfFilm DeleteActorOfFilm(int idFilm);

        //Method search ActorOfFilm by ActorOfFilmName
       // IEnumerable<ActorOfFilm> Search(int idFilm);

       


    }
}
