﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface ProducersInterface
    {
        //Method to get Producer by ID 
        Producer GetAllProducerById(int id);

        //Method to get all Producers
        IEnumerable<Producer> GetAllProducer();

        //Method to add new Producer
        Producer AddProducer(Producer producer);

        //Method to edit Producer information
        Producer EditProducer(Producer producer);

        //Method to delete Producer
        Producer DeleteProducer(Producer producer);

        //Method search Prodcut by ProducerName
        IEnumerable<Producer> Search(string producerName);

        //Method for Filter Producer

        bool ProducerIdentity(Producer value);

    }
}
