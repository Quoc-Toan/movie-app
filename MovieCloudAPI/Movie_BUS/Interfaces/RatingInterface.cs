﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface RatingInterface
    {
        //Method to get Rating by ID 
        Rating GetAllRatingById(int id);

        //Method to get all Ratings
        IEnumerable<Rating> GetAllRating();

        //Method to add new Rating
        Rating AddRating(Rating rating);

        //Method to edit Rating information
        Rating EditRating(Rating rating);

        //Method to delete Rating
        Rating DeleteRating(Rating rating);

        //Method search Prodcut by RatingName
        IEnumerable<Rating> SearchByIdAccount(int idAccount);

      

    }
}
