﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface CountriesInterface
    {
        //Method to get Country by ID 
        Country GetAllCountryById(int id);

        //Method to get all Countrys
        IEnumerable<Country> GetAllCountry();

        //Method to add new Country
        Country AddCountry(Country country);

        //Method to edit Country information
        Country EditCountry(Country country);

        //Method to delete Country
        Country DeleteCountry(Country country);

        //Method search Prodcut by CountryName
        IEnumerable<Country> Search(string countryName);

        //Method for Filter Country

        Boolean CountryIdentity(Country country);

    }
}
