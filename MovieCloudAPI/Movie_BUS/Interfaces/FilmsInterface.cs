﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
    public interface FilmsInterface
    {
        //Method to get Film by ID 
        Film GetAllFilmById(int id);

        //Method to get all Films
        IEnumerable<Film> GetAllFilm();

        //Method to add new Film
        Film AddFilm(Film film);

        //Method to edit Film information
        Film EditFilm(Film film);

        //Method to delete Film
        Film DeleteFilm(Film film);

        //Method search Prodcut by FilmName
        IEnumerable<Film> Search(string filmName);

        //Method for Filter Film

        bool FilmIdentity(Film value);
    }
}
