﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface CategoriesInterface
    {
        //Method to get Category by ID 
        Category GetAllCategoryById(int id);

        //Method to get all Categorys
        IEnumerable<Category> GetAllCategory();

        //Method to add new Category
        Category AddCategory(Category category);

        //Method to edit Category information
        Category EditCategory(Category category);

        //Method to delete Category
        Category DeleteCategory(Category category);

        //Method search Prodcut by CategoryName
        IEnumerable<Category> Search(string categoryName);

        //Method for Filter Category

        bool CategoryIdentity(Category value);


    }
}
