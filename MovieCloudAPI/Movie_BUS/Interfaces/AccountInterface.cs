﻿using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_BUS.Interfaces
{
   public interface AccountInterface
    {
        //Method to get account by ID 
        Account GetAllAccountById(int id);

        //Method to get all accounts
        IEnumerable<Account> GetAllAccount();

        //Method to add new account
        Account AddAccount(Account account);

        //Method to edit account information
        Account EditAccount(Account account);

        //Method to delete account
        Account DeleteAccount(Account account);

        //Method search account by accountName
        IEnumerable<Account> Search(string accountName);

        Boolean AccountIdentity(Account value);



    }
}
