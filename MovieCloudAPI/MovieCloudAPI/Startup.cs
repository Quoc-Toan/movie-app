using Actor_BUS.Service;
using Category_BUS.Service;
using Comment_BUS.Service;
using Country_BUS.Service;
using Director_BUS.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Movie_BUS.Interfaces;
using Movie_BUS.Service;
using Movie_DAO.DBContext;
using Producer_BUS.Service;
using Rating_BUS.Service;
using System.IO;

namespace MovieCloudAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MovieCloudAPI", Version = "v1" });
            });

            services.AddDbContext<FilmsDBContext>(optionns =>
               optionns.UseSqlServer(Configuration.GetConnectionString("DevConnection")));

            services.AddScoped<IdentifyInterface, IdentifyServices>();
            services.AddScoped<AccountInterface, AccountsServices>();
            services.AddScoped<ActorsInterface, ActorsServices>();
            services.AddScoped<CategoriesInterface, CategorysService>();
            services.AddScoped<CountriesInterface, CountriesService>();
            services.AddScoped<DirectorsInterface, DirectorsServices>();
            services.AddScoped<ViewHistoryInterface, ViewHistoriesServices>();
            services.AddScoped<FilmsInterface, MoviesServices>();
            services.AddScoped<ProducersInterface, ProducersServices>();
            services.AddScoped<ActorOfFilmInterface, ActorOfFilmsServices>();
            services.AddScoped<DirectorOfFilmInterface, DirectorOfFilmsServices>();
            services.AddScoped<CommentInterface, CommentsService>();
            services.AddScoped<RatingInterface, RatingsService>();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MovieCloudAPI v1"));
            }

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                     Path.Combine(Directory.GetCurrentDirectory(), "Data")),
                RequestPath = "/Data",
                EnableDefaultFiles = true
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
