﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using Movie_DAO.Models.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentifyController : ControllerBase
    {
        private readonly IdentifyInterface _service;

        public IdentifyController(IdentifyInterface service)
        {
            _service = service;
        }


        [Route("/login")]
        [HttpPost]
        public bool IdentityAcc(LoginAccount value)
        {
            return _service.AccountIdentity(value);
        }

        [Route("/register")]
        [HttpPost]
        public ActionResult<Account> Register(Account account)
        {
            _service.AddProduct(account);//call AddProduct() method to add new product
            return Ok(account);
        }

    }
}
