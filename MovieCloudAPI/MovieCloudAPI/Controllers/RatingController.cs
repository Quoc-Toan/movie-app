﻿using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System.Collections.Generic;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly RatingInterface _service;

        public RatingController(RatingInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Rating
        * 
        * Return a list information of Rating
        */
        [Route("/Rating/Rating-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Rating>> GetAllRating()
        {
            var ApprItems = _service.GetAllRating();//call GetAllRating() to gets all Rating
            return Ok(ApprItems);//return the list
        }


        /**
         * Post API
         * 
         * API to add new Rating
         */
        [Route("/Rating/Rating-appr-create")]
        [HttpPost]
        public ActionResult<Rating> PostRating(Rating Rating)
        {
            _service.AddRating(Rating);//call AddRating() method to add new Rating
            return Ok(Rating);
        }
        /**
         * Search API
         *
         **/
        [Route("/Rating/Rating-appr-search/{idAccount}")]
        [HttpGet]
        public ActionResult<Rating> Search(int idAccount)
        {
            var ApprItems = _service.SearchByIdAccount(idAccount);//call method Search to search Rating by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Rating on database
         **/
        [Route("/Rating/Rating-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Rating> DeleteRating(int id)
        {
            var check = _service.GetAllRatingById(id); //call GetAllRatingById() to find out Rating you want to delete
            if (check != null)//if the Rating is existed
            {
                _service.DeleteRating(check);//call DeleteRating to delete that Rating
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Rating on database
         */
        [Route("/Rating/Rating-appr-edit")]
        [HttpPut]
        public ActionResult<Rating> EditRating(Rating Rating)
        {
            _service.EditRating(Rating);//call EditRating() method to edit information of Rating
            return Ok();
        }

      

    }
}
