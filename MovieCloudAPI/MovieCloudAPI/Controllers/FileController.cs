﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 4294967295)]
        [RequestSizeLimit(4294967295)]
        public IActionResult Upload
        (IFormFile file)
        {
            if(file!= null)
            {
                string fileName = $"..\\MovieCloudAPI\\Data\\Images\\{file.FileName}";

                using (FileStream fs = System.IO.File.Create(fileName))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }



                return Ok();
            }
            return Ok();
        }

        [HttpGet("files")]
        public async Task<ActionResult> DownloadFile(string name)
        {
            var filePath = $"..\\MovieCloudAPI\\Data\\Images\\{name}"; // Here, you should validate the request and the existance of the file.

            var bytes = await System.IO.File.ReadAllBytesAsync(filePath);
            return File(bytes, "text/plain", Path.GetFileName(filePath));
        }


    }
    
}
