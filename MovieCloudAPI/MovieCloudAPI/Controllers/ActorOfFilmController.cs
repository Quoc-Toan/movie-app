﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorOfFilmController : ControllerBase
    {
        private readonly ActorOfFilmInterface _service;

        public ActorOfFilmController(ActorOfFilmInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all ActorOfFilm
        * 
        * Return a list information of ActorOfFilm
        */
        [Route("/ActorOfFilm/ActorOfFilm-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<ActorOfFilm>> GetAllActorOfFilm()
        {
            var ApprItems = _service.GetAllActorOfFilm();//call GetAllActorOfFilm() to gets all ActorOfFilm
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new ActorOfFilm
         */
        [Route("/ActorOfFilm/ActorOfFilm-appr-create")]
        [HttpPost]
        public ActionResult<ActorOfFilm> PostActorOfFilm(List<ActorOfFilm> actorOfFilm)
        {
            _service.AddActorOfFilm(actorOfFilm);//call AddActorOfFilm() method to add new ActorOfFilm
            return Ok(actorOfFilm);
        }
     
        /**
         * Delete API
         * 
         * API to delete ActorOfFilm on database
         **/
        [Route("/ActorOfFilm/ActorOfFilm-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<ActorOfFilm> DeleteActorOfFilm(int id)
        {
            try
            {
                _service.DeleteActorOfFilm(id);//call DeleteActorOfFilm to delete that ActorOfFilm
                return Ok();
            }
            catch { 
            return NotFound();//if the id is not found, print out error
        }
        }

        /**
         * Put API
         * 
         * The API to edit existed ActorOfFilm on database
         */
        [Route("/ActorOfFilm/ActorOfFilm-appr-edit")]
        [HttpPut]
        public ActionResult<ActorOfFilm> EditActorOfFilm(ActorOfFilm ActorOfFilm)
        {
            _service.EditActorOfFilm(ActorOfFilm);//call EditActorOfFilm() method to edit information of ActorOfFilm
            return Ok();
        }

       
    }
}
