﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorOfFilmController : ControllerBase
    {
        private readonly DirectorOfFilmInterface _service;

        public DirectorOfFilmController(DirectorOfFilmInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all DirectorOfFilm
        * 
        * Return a list information of DirectorOfFilm
        */
        [Route("/DirectorOfFilm/DirectorOfFilm-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<DirectorOfFilm>> GetAllDirectorOfFilm()
        {
            var ApprItems = _service.GetAllDirectorOfFilm();//call GetAllDirectorOfFilm() to gets all DirectorOfFilm
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new DirectorOfFilm
         */
        [Route("/DirectorOfFilm/DirectorOfFilm-appr-create")]
        [HttpPost]
        public ActionResult<DirectorOfFilm> PostDirectorOfFilm(List<DirectorOfFilm> DirectorOfFilm)
        {
            _service.AddDirectorOfFilm(DirectorOfFilm);//call AddDirectorOfFilm() method to add new DirectorOfFilm
            return Ok(DirectorOfFilm);
        }
     
        /**
         * Delete API
         * 
         * API to delete DirectorOfFilm on database
         **/
        [Route("/DirectorOfFilm/DirectorOfFilm-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<DirectorOfFilm> DeleteDirectorOfFilm(int id)
        {
            try
            {
                _service.DeleteDirectorOfFilm(id);//call DeleteDirectorOfFilm to delete that DirectorOfFilm
                return Ok();
            }
            catch { 
            return NotFound();//if the id is not found, print out error
        }
        }

        /**
         * Put API
         * 
         * The API to edit existed DirectorOfFilm on database
         */
        [Route("/DirectorOfFilm/DirectorOfFilm-appr-edit")]
        [HttpPut]
        public ActionResult<DirectorOfFilm> EditDirectorOfFilm(DirectorOfFilm DirectorOfFilm)
        {
            _service.EditDirectorOfFilm(DirectorOfFilm);//call EditDirectorOfFilm() method to edit information of DirectorOfFilm
            return Ok();
        }

       
    }
}
