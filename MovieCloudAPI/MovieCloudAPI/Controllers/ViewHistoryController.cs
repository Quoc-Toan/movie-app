﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViewHistoryController : ControllerBase
    {
        private readonly ViewHistoryInterface _service;

        public ViewHistoryController(ViewHistoryInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all ViewHistory
        * 
        * Return a list information of ViewHistory
        */
        [Route("/ViewHistory/ViewHistory-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<ViewHistory>> GetAllViewHistory()
        {
            var ApprItems = _service.GetAllViewHistory();//call GetAllViewHistory() to gets all ViewHistory
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new ViewHistory
         */
        [Route("/ViewHistory/ViewHistory-appr-create")]
        [HttpPost]
        public ActionResult<ViewHistory> PostViewHistory(ViewHistory ViewHistory)
        {
            _service.AddViewHistory(ViewHistory);//call AddViewHistory() method to add new ViewHistory
            return Ok(ViewHistory);
        }
        /**
         * Search API
         *
         **/
        [Route("/ViewHistory/ViewHistory-appr-getByIdAccount/{id}")]
        [HttpGet]
        public ActionResult<ViewHistory> GetByIdAccount(int id)
        {
            var ApprItems = _service.GetByIdAccount(id);//call method Search to search ViewHistory by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete ViewHistory on database
         **/
        [Route("/ViewHistory/ViewHistory-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<ViewHistory> DeleteViewHistory(int id)
        {
            var check = _service.GetAllViewHistoryById(id); //call GetAllViewHistoryById() to find out ViewHistory you want to delete
            if (check != null)//if the ViewHistory is existed
            {
                _service.DeleteViewHistory(check);//call DeleteViewHistory to delete that ViewHistory
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed ViewHistory on database
         */
        [Route("/ViewHistory/ViewHistory-appr-edit")]
        [HttpPut]
        public ActionResult<ViewHistory> EditViewHistory(ViewHistory ViewHistory)
        {
            _service.EditViewHistory(ViewHistory);//call EditViewHistory() method to edit information of ViewHistory
            return Ok();
        }

        
    }
}
