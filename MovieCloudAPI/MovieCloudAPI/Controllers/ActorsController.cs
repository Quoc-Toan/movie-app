﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly ActorsInterface _service;

        public ActorController(ActorsInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Actors
        * 
        * Return a list information of Actors
        */
        [Route("/Actors/Actors-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Actor>> GetAllActor()
        {
            var ApprItems = _service.GetAllActor();//call GetAllActor() to gets all Actors
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Actors
         */
        [Route("/Actors/Actors-appr-create")]
        [HttpPost]
        public ActionResult<Actor> PostActor(Actor Actor)
        {
            _service.AddActor(Actor);//call AddActor() method to add new Actors
            return Ok(Actor);
        }
        /**
         * Search API
         *
         **/
        [Route("/Actors/Actors-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Actor> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Actors by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Actors on database
         **/
        [Route("/Actors/Actors-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Actor> DeleteActor(int id)
        {
            var check = _service.GetAllActorById(id); //call GetAllActorById() to find out Actors you want to delete
            if (check != null)//if the Actors is existed
            {
                _service.DeleteActor(check);//call DeleteActor to delete that Actors
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Actors on database
         */
        [Route("/Actors/Actors-appr-edit")]
        [HttpPut]
        public ActionResult<Actor> EditActor(Actor Actor)
        {
            _service.EditActor(Actor);//call EditActor() method to edit information of Actors
            return Ok();
        }

        [Route("/Actors/Actors-Exists")]
        [HttpPost]
        public bool IdentityAct(Actor Actor)
        {
            return _service.ActorIdentity(Actor);
        }


    }
}
