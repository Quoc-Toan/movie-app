﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmController : ControllerBase
    {

        private readonly FilmsInterface _service;

        public FilmController(FilmsInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Film
        * 
        * Return a list information of Film
        */
        [Route("/Film/Film-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Film>> GetAllFilm()
        {
            var ApprItems = _service.GetAllFilm();//call GetAllFilm() to gets all Film
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Film
         */
        [Route("/Film/Film-appr-create")]
        [HttpPost]
        public ActionResult<Film> PostFilm(Film film)
        {
            _service.AddFilm(film);//call AddFilm() method to add new Film
            return Ok(film);
        }
        /**
         * Search API
         *
         **/
        [Route("/Film/Film-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Film> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Film by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Film on database
         **/
        [Route("/Film/Film-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Film> DeleteFilm(int id)
        {
            var check = _service.GetAllFilmById(id); //call GetAllFilmById() to find out Film you want to delete
            if (check != null)//if the Film is existed
            {
                _service.DeleteFilm(check);//call DeleteFilm to delete that Film
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Film on database
         */
        [Route("/Film/Film-appr-edit")]
        [HttpPut]
        public ActionResult<Film> EditFilm(Film Film)
        {
            _service.EditFilm(Film);//call EditFilm() method to edit information of Film
            return Ok();
        }

        [Route("/Film/Film-Exists")]
        [HttpPost]
        public bool IdentityFilm(Film Film)
        {
            return _service.FilmIdentity(Film);
        }


    }
}
