﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoriesInterface _service;

        public CategoryController(CategoriesInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Category
        * 
        * Return a list information of Category
        */
        [Route("/Category/Category-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetAllCategory()
        {
            var ApprItems = _service.GetAllCategory();//call GetAllCategory() to gets all Category
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Category
         */
        [Route("/Category/Category-appr-create")]
        [HttpPost]
        public ActionResult<Category> PostCategory(Category Category)
        {
            _service.AddCategory(Category);//call AddCategory() method to add new Category
            return Ok(Category);
        }
        /**
         * Search API
         *
         **/
        [Route("/Category/Category-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Category> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Category by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Category on database
         **/
        [Route("/Category/Category-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Category> DeleteCategory(int id)
        {
            var check = _service.GetAllCategoryById(id); //call GetAllCategoryById() to find out Category you want to delete
            if (check != null)//if the Category is existed
            {
                _service.DeleteCategory(check);//call DeleteCategory to delete that Category
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Category on database
         */
        [Route("/Category/Category-appr-edit")]
        [HttpPut]
        public ActionResult<Category> EditCategory(Category Category)
        {
            _service.EditCategory(Category);//call EditCategory() method to edit information of Category
            return Ok();
        }

        [Route("/Category/Category-Exists")]
        [HttpPost]
        public bool IdentityCate(Category Category)
        {
            return _service.CategoryIdentity(Category);
        }


    }
}
