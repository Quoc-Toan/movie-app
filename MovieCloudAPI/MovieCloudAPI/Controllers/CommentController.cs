﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly CommentInterface _service;

        public CommentController(CommentInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Comment
        * 
        * Return a list information of Comment
        */
        [Route("/Comment/Comment-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Comment>> GetAllComment()
        {
            var ApprItems = _service.GetAllComment();//call GetAllComment() to gets all Comment
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Comment
         */
        [Route("/Comment/Comment-appr-create")]
        [HttpPost]
        public ActionResult<Comment> PostComment(Comment Comment)
        {
            _service.AddComment(Comment);//call AddComment() method to add new Comment
            return Ok(Comment);
        }
        /**
         * Search API
         *
         **/
        [Route("/Comment/Comment-appr-search/{idAccount}")]
        [HttpGet]
        public ActionResult<Comment> Search(int idAccount)
        {
            var ApprItems = _service.SearchByIdAccount(idAccount);//call method Search to search Comment by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Comment on database
         **/
        [Route("/Comment/Comment-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Comment> DeleteComment(int id)
        {
            var check = _service.GetAllCommentById(id); //call GetAllCommentById() to find out Comment you want to delete
            if (check != null)//if the Comment is existed
            {
                _service.DeleteComment(check);//call DeleteComment to delete that Comment
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Comment on database
         */
        [Route("/Comment/Comment-appr-edit")]
        [HttpPut]
        public ActionResult<Comment> EditComment(Comment Comment)
        {
            _service.EditComment(Comment);//call EditComment() method to edit information of Comment
            return Ok();
        }

      

    }
}
