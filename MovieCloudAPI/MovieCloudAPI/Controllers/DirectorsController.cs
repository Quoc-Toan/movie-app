﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        private readonly DirectorsInterface _service;

        public DirectorController(DirectorsInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Director
        * 
        * Return a list information of Director
        */
        [Route("/Director/Director-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Director>> GetAllDirector()
        {
            var ApprItems = _service.GetAllDirector();//call GetAllDirector() to gets all Director
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Director
         */
        [Route("/Director/Director-appr-create")]
        [HttpPost]
        public ActionResult<Director> PostDirector(Director Director)
        {
            _service.AddDirector(Director);//call AddDirector() method to add new Director
            return Ok(Director);
        }
        /**
         * Search API
         *
         **/
        [Route("/Director/Director-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Director> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Director by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Director on database
         **/
        [Route("/Director/Director-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Director> DeleteDirector(int id)
        {
            var check = _service.GetAllDirectorById(id); //call GetAllDirectorById() to find out Director you want to delete
            if (check != null)//if the Director is existed
            {
                _service.DeleteDirector(check);//call DeleteDirector to delete that Director
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Director on database
         */
        [Route("/Director/Director-appr-edit")]
        [HttpPut]
        public ActionResult<Director> EditDirector(Director Director)
        {
            _service.EditDirector(Director);//call EditDirector() method to edit information of Director
            return Ok();
        }

        [Route("/Director/Director-Exists")]
        [HttpPost]
        public bool IdentityDir(Director Director)
        {
            return _service.DirectorIdentity(Director);
        }



    }
}
