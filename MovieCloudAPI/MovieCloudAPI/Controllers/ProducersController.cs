﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProducerController : ControllerBase
    {
        private readonly ProducersInterface _service;

        public ProducerController(ProducersInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Producer
        * 
        * Return a list information of Producer
        */
        [Route("/Producer/Producer-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Producer>> GetAllProducer()
        {
            var ApprItems = _service.GetAllProducer();//call GetAllProducer() to gets all Producer
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Producer
         */
        [Route("/Producer/Producer-appr-create")]
        [HttpPost]
        public ActionResult<Producer> PostProducer(Producer Producer)
        {
            _service.AddProducer(Producer);//call AddProducer() method to add new Producer
            return Ok(Producer);
        }
        /**
         * Search API
         *
         **/
        [Route("/Producer/Producer-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Producer> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Producer by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Producer on database
         **/
        [Route("/Producer/Producer-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Producer> DeleteProducer(int id)
        {
            var check = _service.GetAllProducerById(id); //call GetAllProducerById() to find out Producer you want to delete
            if (check != null)//if the Producer is existed
            {
                _service.DeleteProducer(check);//call DeleteProducer to delete that Producer
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Producer on database
         */
        [Route("/Producer/Producer-appr-edit")]
        [HttpPut]
        public ActionResult<Producer> EditProducer(Producer Producer)
        {
            _service.EditProducer(Producer);//call EditProducer() method to edit information of Producer
            return Ok();
        }

        [Route("/Producer/Producer-Exists")]
        [HttpPost]
        public bool IdentityPro(Producer Producer)
        {
            return _service.ProducerIdentity(Producer);
        }


    }
}
