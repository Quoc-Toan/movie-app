﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly CountriesInterface _service;

        public CountryController(CountriesInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all Country
        * 
        * Return a list information of Country
        */
        [Route("/Country/Country-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Country>> GetAllCountry()
        {
            var ApprItems = _service.GetAllCountry();//call GetAllCountry() to gets all Country
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Country
         */
        [Route("/Country/Country-appr-create")]
        [HttpPost]
        public ActionResult<Country> PostCountry(Country Country)
        {
            _service.AddCountry(Country);//call AddCountry() method to add new Country
            return Ok(Country);
        }
        /**
         * Search API
         *
         **/
        [Route("/Country/Country-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Country> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Country by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Country on database
         **/
        [Route("/Country/Country-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Country> DeleteCountry(int id)
        {
            var check = _service.GetAllCountryById(id); //call GetAllCountryById() to find out Country you want to delete
            if (check != null)//if the Country is existed
            {
                _service.DeleteCountry(check);//call DeleteCountry to delete that Country
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Country on database
         */
        [Route("/Country/Country-appr-edit")]
        [HttpPut]
        public ActionResult<Country> EditCountry(Country Country)
        {
            _service.EditCountry(Country);//call EditCountry() method to edit information of Country
            return Ok();
        }

        [Route("/Country/Country-Exists")]
        [HttpPost]
        public bool IdentityCoun(Country Country)
        {
            return _service.CountryIdentity(Country);
        }

    }
}
