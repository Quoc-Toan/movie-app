﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_BUS.Interfaces;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviCloudAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountInterface _service;

        public AccountController(AccountInterface service)
        {
            _service = service;
        }

        /**
        * Get API to gets all account
        * 
        * Return a list information of Account
        */
        [Route("/Account/Account-appr-list")]
        [HttpGet]
        public ActionResult<IEnumerable<Account>> GetAllAccount()
        {
            var ApprItems = _service.GetAllAccount();//call GetAllAccount() to gets all account
            return Ok(ApprItems);//return the list
        }

        /**
         * Post API
         * 
         * API to add new Account
         */
        [Route("/Account/Account-appr-create")]
        [HttpPost]
        public ActionResult<Account> PostAccount(Account account)
        {
            try
            {
                _service.AddAccount(account);//call AddAccount() method to add new Account
            }
            catch (Exception)
            {

                return NotFound();
            }
            return Ok(account);
        }
        /**
         * Search API
         *
         **/
        [Route("/Account/Account-appr-search/{nameSearch}")]
        [HttpGet]
        public ActionResult<Account> Search(string nameSearch)
        {
            var ApprItems = _service.Search(nameSearch);//call method Search to search Account by name
            return Ok(ApprItems);
        }

        /**
         * Delete API
         * 
         * API to delete Account on database
         **/
        [Route("/Account/Account-appr-delete/{id}")]
        [HttpDelete]
        public ActionResult<Account> DeleteAccount(int id)
        {
            var check = _service.GetAllAccountById(id); //call GetAllAccountById() to find out Account you want to delete
            if (check != null)//if the Account is existed
            {
                _service.DeleteAccount(check);//call DeleteAccount to delete that Account
                return Ok();
            }
            return NotFound();//if the id is not found, print out error
        }

        /**
         * Put API
         * 
         * The API to edit existed Account on database
         */
        [Route("/Account/Account-appr-edit")]
        [HttpPut]
        public ActionResult<Account> EditAccount(Account account)
        {
            _service.EditAccount(account);//call EditAccount() method to edit information of Account
            return Ok();
        }

        [Route("/Account/Account-Exists")]
        [HttpPost]
        public bool IdentityAcc(Account account)
        {
            return _service.AccountIdentity(account);
        }

    }
}
