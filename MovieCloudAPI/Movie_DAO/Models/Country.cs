﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Country
    {
        [Key]
        public int idCountry { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String countryName { get; set; }
    }
}
