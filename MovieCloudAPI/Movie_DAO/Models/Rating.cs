﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Rating
    {
        [Key]
        public int idRating { get; set; }
        public int idAccount { get; set; }
        public int idFilm { get; set; }
        public int rating { get; set; }
        

    }
}
