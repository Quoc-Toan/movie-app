﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Actor
    {
        [Key]
        public int idActor { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String actorName { get; set; }

        [Column(TypeName = "date")]
        public DateTime birthday { get; set; }
        public int gender { get; set; }
        public int idCountry { get; set; }
    }
}
