﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Category
    {
        [Key]
        public int idCategory { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String categoryName { get; set; }
    }
}
