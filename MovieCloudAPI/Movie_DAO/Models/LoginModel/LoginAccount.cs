﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Models.LoginModel
{
   public class LoginAccount
    {
        public String username { get; set; }
        public String password { get; set; }
    }
}
