﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Film
    {
        [Key]
        public int idFilm { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String filmName { get; set; }
        public int idProducer { get; set; }
        public int idCategory { get; set; }
        public int idCountry { get; set; }
        public int view { get; set; }
        public double rate { get; set; }

        [Column(TypeName = "date")]
        public DateTime releaseDate { get; set; }
        public int limitAge { get; set; }
        public bool status { get; set; }

        public String urlFilm { get; set; }
        public String detailFilm { get; set; }
        public String urlPoster { get; set; }
    }
}
