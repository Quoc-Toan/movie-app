﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Comment
    {
        [Key]
        public int idComment { get; set; }
        public int idAccount { get; set; }
        public int idFilm { get; set; }

        [Column(TypeName = "varchar(500)")]
        public String content { get; set; }

        [Column(TypeName = "date")]
        public DateTime createdDay { get; set; }




    }
}
