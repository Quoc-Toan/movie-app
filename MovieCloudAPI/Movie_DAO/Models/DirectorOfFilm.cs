﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class DirectorOfFilm
    {
        [Key]
        public int idDirectorOfFilm { get; set; }
        public int idDirector { get; set; }
        public int idFilm { get; set; }
        

    }
}
