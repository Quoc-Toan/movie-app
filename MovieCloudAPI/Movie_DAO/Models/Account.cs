﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class Account
    {
        [Key]
        public int idAccount { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String username { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public String password { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String  fullName { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public String email { get; set; }

        
        public int phone { get; set; }
        public int role { get; set; }
        public bool status { get; set; }

        [Column(TypeName = "nvarchar(200)")]
        public String urlAvatar { get; set; }
    }
}
