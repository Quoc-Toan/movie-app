﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.Model
{
    public class ActorOfFilm
    {
        [Key]
        public int idActorOfFilm { get; set; }
        public int idActor { get; set; }
        public int idFilm { get; set; }
        

    }
}
