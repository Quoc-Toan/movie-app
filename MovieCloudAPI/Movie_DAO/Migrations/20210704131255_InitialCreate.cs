﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Movie_DAO.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    idAccount = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    username = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    password = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    fullName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    phone = table.Column<int>(type: "int", nullable: true),
                    role = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: true),
                    urlAvatar = table.Column<string>(type: "nvarchar(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.idAccount);
                });

            migrationBuilder.CreateTable(
                name: "ActorOfFilms",
                columns: table => new
                {
                    idActorOfFilm = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idActor = table.Column<int>(type: "int", nullable: true),
                    idFilm = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActorOfFilms", x => x.idActorOfFilm);
                });

            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    idActor = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    actorName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    birthday = table.Column<DateTime>(type: "date", nullable: true),
                    gender = table.Column<int>(type: "int", nullable: true),
                    idCountry = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.idActor);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    idCategory = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    categoryName = table.Column<string>(type: "nvarchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.idCategory);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    idComment = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idAccount = table.Column<int>(type: "int", nullable: true),
                    idFilm = table.Column<int>(type: "int", nullable: true),
                    content = table.Column<string>(type: "varchar(500)", nullable: true),
                    createdDay = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.idComment);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    idCountry = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    countryName = table.Column<string>(type: "nvarchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.idCountry);
                });

            migrationBuilder.CreateTable(
                name: "DirectorOfFilms",
                columns: table => new
                {
                    idDirectorOfFilm = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idDirector = table.Column<int>(type: "int", nullable: true),
                    idFilm = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectorOfFilms", x => x.idDirectorOfFilm);
                });

            migrationBuilder.CreateTable(
                name: "Directors",
                columns: table => new
                {
                    idDirector = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    directorName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    birthday = table.Column<DateTime>(type: "date", nullable: true),
                    gender = table.Column<int>(type: "int", nullable: true),
                    idCountry = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directors", x => x.idDirector);
                });

            migrationBuilder.CreateTable(
                name: "Films",
                columns: table => new
                {
                    idFilm = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    filmName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    idProducer = table.Column<int>(type: "int", nullable: true),
                    idCategory = table.Column<int>(type: "int", nullable: true),
                    idCountry = table.Column<int>(type: "int", nullable: true),
                    view = table.Column<int>(type: "int", nullable: true),
                    rate = table.Column<double>(type: "float", nullable: true),
                    releaseDate = table.Column<DateTime>(type: "date", nullable: true),
                    limitAge = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: true),
                    urlFilm = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    detailFilm = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    urlPoster = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Films", x => x.idFilm);
                });

            migrationBuilder.CreateTable(
                name: "Producers",
                columns: table => new
                {
                    idProducer = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    producerName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    idCountry = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producers", x => x.idProducer);
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    idRating = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idAccount = table.Column<int>(type: "int", nullable: true),
                    idFilm = table.Column<int>(type: "int", nullable: true),
                    rating = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => x.idRating);
                });

            migrationBuilder.CreateTable(
                name: "ViewHistories",
                columns: table => new
                {
                    idViewHistory = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idFilm = table.Column<int>(type: "int", nullable: true),
                    idAccount = table.Column<int>(type: "int", nullable: true),
                    minues = table.Column<int>(type: "int", nullable: true),
                    createdDay = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViewHistories", x => x.idViewHistory);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "ActorOfFilms");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "DirectorOfFilms");

            migrationBuilder.DropTable(
                name: "Directors");

            migrationBuilder.DropTable(
                name: "Films");

            migrationBuilder.DropTable(
                name: "Producers");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "ViewHistories");
        }
    }
}
