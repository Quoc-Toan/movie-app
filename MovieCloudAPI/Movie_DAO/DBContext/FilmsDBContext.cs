﻿using Microsoft.EntityFrameworkCore;
using Movie_DAO.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_DAO.DBContext
{
    public class FilmsDBContext: DbContext
    {
        public FilmsDBContext(DbContextOptions<FilmsDBContext> options) : base(options)
        { }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<ViewHistory> ViewHistories { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<DirectorOfFilm> DirectorOfFilms { get; set; }
        public DbSet<ActorOfFilm> ActorOfFilms { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rating> Ratings { get; set; }
    }
}
