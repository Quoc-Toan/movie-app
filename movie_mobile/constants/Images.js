const Onboarding = 'https://images.unsplash.com/photo-1505995433366-e12047f3f144?fit=crop&w=840&q=80';
const Pro = 'https://hc.com.vn/i/ecommerce/media/GS.006085_FEATURE_71021.jpg';
const Products = {
  'Accessories': 'https://source.unsplash.com//l1MCA0VyNrk/840x840',
};

const Profile = 'https://rockandbluescruise.com/wp-content/uploads/2020/10/tieu-su-ronaldo.jpg';
const Avatar = 'https://images.unsplash.com/photo-1518725522904-4b3939358342?fit=crop&w=210&q=80';
const Logo = require('../assets/images/logo.jpg');
const uri = 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQA_-tL18_rj9zEcjN6n41NEaJm-kRNF9UeOtvksZ4z_OW6jRA9';

const Viewed = [
  'https://rapchieuphim.com/photos/3/phim-godzilla-king-of-the-monsters/phim-godzilla-king-of-the-monsters-2.jpg',
  'https://images.unsplash.com/photo-1497034825429-c343d7c6a68f?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1487376480913-24046456a727?fit=crop&w=240&q=80',
];

const Action = [
  'https://rapchieuphim.com/photos/3/phim-godzilla-king-of-the-monsters/phim-godzilla-king-of-the-monsters-2.jpg',
  'https://static.247phim.com/146488/conversions/5ebe00358c6aa_6_underground_d-435_627.jpg',
  'https://storage.googleapis.com/stc.zcdn.link/hdf/8/b9/1738228317631032066/1572976851290-poster-org-fast_furios_2019_poster.jpg',
  'https://vtv1.mediacdn.vn/thumb_w/640/2018/4/18/24-15240107570092087133215.jpg'
];

export default {
  Onboarding,
  Pro,
  Products,
  Profile,
  Viewed,
  Action,
  Avatar,
  Logo,
  uri
}