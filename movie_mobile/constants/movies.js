const movies = [
    {
      id: '1',
      name: '6 UNDERGROUND',
      location: 'Green street,Central district',
      image: require('../assets/images/6.jpg'),
      star: '8',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
    {
      id: '2',
      name: 'HOOBS VS SHAW',
      location: 'Yuki street',
      image: require('../assets/images/Hobbs.jpg'),
      star: '9',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
    {
      id: '3',
      name: 'MEN IN BLACK',
      location: 'Almond street',
      image: require('../assets/images/MIB.jpg'),
      star: '10',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
    {
      id: '4',
      name: 'BAD BOY FOR LIFE',
      location: 'Main street',
      image: require('../assets/images/Badboys.jpg'),
      star: '7',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
    {
      id: '5',
      name: 'BAD BOY FOR LIFE',
      location: 'Main street',
      image: require('../assets/images/Badboys.jpg'),
      star: '7',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
    {
      id: '6',
      name: 'MEN IN BLACK',
      location: 'Almond street',
      image: require('../assets/images/MIB.jpg'),
      star: '10',
      details: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id leo. Velit euismod in pellentesque massa placerat duis ultricies lacus sed. Justo laoreet sit amet cursus sit`,
    },
  ];
  
  export default movies;