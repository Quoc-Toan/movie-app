import React,  { useState } from 'react';
import { withNavigation } from '@react-navigation/compat';
import { TouchableOpacity, StyleSheet, Platform, ScrollView, Dimensions, View } from 'react-native';
import { Button, Block, NavBar, Input, Text, theme } from 'galio-framework';

import Icon from './Icon';
import materialTheme from '../constants/Theme';

const { height, width } = Dimensions.get('window');
const iPhoneX = () => Platform.OS === 'ios' && (height === 812 || width === 812 || height === 896 || width === 896);

const SearchButton = ({isWhite, style, navigation}) => (
  <TouchableOpacity style={[styles.button, style]}>
    <Icon
      size={16}
      family="entypo"
      name="magnifying-glass"
      color={theme.COLORS[isWhite ? 'WHITE' : 'ICON']}
    />
  </TouchableOpacity>
);

class Header extends React.Component {

  handleLeftPress = () => {
    const { back, navigation } = this.props;
    return (back ? navigation.goBack() : navigation.openDrawer());
  }

  // renderRight = () => {
  //   const { search, tabs } = this.props;
  //   <Block cen>
  //       {search ? this.renderSearch() : null}
  //   </Block>
    // if (title === 'Title') {
    //   return [
    //     <ChatButton key='chat-title' navigation={navigation} isWhite={white} />,
    //   ]
    // }

    // switch (title) {
    //   case 'Home':
    //     return ([
    //       <ChatButton key='chat-home' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Deals':
    //     return ([
    //       <ChatButton key='chat-categories' navigation={navigation} />,
    //     ]);
    //   case 'Products':
    //     return ([
    //       <ChatButton key='chat-products' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Product Detail':
    //     return ([
    //       <ChatButton key='chat-product-detail' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Profile':
    //     return ([
    //       <ChatButton key='chat-profile' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Product':
    //     return ([
    //       <SearchButton key='search-product' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Search':
    //     return ([
    //       <ChatButton key='chat-search' navigation={navigation} isWhite={white} />,
    //     ]);
    //   case 'Settings':
    //     return ([
    //       <ChatButton key='chat-search' navigation={navigation} isWhite={white} />,
    //     ]);
    //   default:
    //     break;
    // }
  // }

  renderSearch = () => {
    const { navigation } = this.props;
    return (
      <Input
        style={styles.search} 
        iconContent={<Icon size={16} color={theme.COLORS.BLACK} name="magnifying-glass" family="entypo" placeholder="Search"/>}
      />
    )
  }


  renderTabs = () => {
    const categories = ['Trending', 'Popular', 'Action', 'Featured'];
    
    const CategoryList = () => {
      const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0);
      const { navigation } = this.props;

      return(
        <View style={styles.categoryListContainer}>
            {categories.map((item, index) => (
              <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              onPress={() => setSelectedCategoryIndex(index)}
              >
                <View>
                  <Text size={16} style={{...styles.tabTitle,
                  color:
                    selectedCategoryIndex == index
                      ? materialTheme.COLORS.ACTIVE
                      : materialTheme.COLORS.BLACK,
                  }}>
                  {item}
                  </Text>
                      {selectedCategoryIndex == index && (
                        <View
                          style={{
                            height: 3,
                            width: 70,
                            backgroundColor: materialTheme.COLORS.ACTIVE,
                            marginTop: 2,
                          }}
                        />
                      )}
                </View>
              </TouchableOpacity>  
            ))}
        </View>
      )
    }

    return (
      <Block row style={styles.tabs}>
        <CategoryList/>
      </Block>
    )
  }

  renderHeader = () => {
    const { search, tabs,  } = this.props;
    if (search || tabs) {
      return (
        <Block center>
          {tabs ? this.renderTabs() : null}
        </Block>
      )
    }
    return null;
  }

  render() {
    const { back, title, white, transparent, navigation } = this.props;
    const noShadow = ["Profile"].includes(title);
    const noHeader = ["Sign In", "Sign Up", "Product Detail", "Movie"].includes(title);
    const headerStyles = [
      !noShadow ? styles.shadow : null,
      transparent ? { backgroundColor: 'rgba(0,0,0,0)' } : null,
    ];

    return (
      <Block style={headerStyles}>
        {
          !noHeader ? (
            <NavBar
              back={back}
              title={title}
              style={styles.navbar}
              transparent={transparent}
              right={this.renderSearch()}
              rightStyle={{ alignItems: 'center' }}
              leftStyle={{ flex: 0.3, paddingTop: 2  }}
              leftIconName={(back ? 'chevron-left' : 'navicon')}
              leftIconColor={white ? theme.COLORS.WHITE : theme.COLORS.ICON}
              titleStyle={[
                styles.title,
                {color: theme.COLORS[white ? 'WHITE' : 'ICON']},
              ]}
              onLeftPress={this.handleLeftPress}
            />
          ) : null
        }
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {!noHeader ? this.renderHeader() : null}  
        </ScrollView>  
      </Block>
    );
  }
}

export default withNavigation(Header);

const styles = StyleSheet.create({

  button: {
    padding: 12,
    position: 'relative',
  },

  title: {
    width: '100%',
    fontSize: 16,
    fontWeight: 'bold',
  },

  navbar: {
    paddingVertical: 0,
    paddingBottom: theme.SIZES.BASE * 1.5,
    paddingTop: iPhoneX ? theme.SIZES.BASE * 2 : theme.SIZES.BASE,
    zIndex: 5,
  },

  shadow: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  notify: {
    backgroundColor: materialTheme.COLORS.LABEL,
    borderRadius: 4,
    height: theme.SIZES.BASE / 2,
    width: theme.SIZES.BASE / 2,
    position: 'absolute',
    top: 8,
    right: 8,
  },

  header: {
    backgroundColor: theme.COLORS.WHITE,
  },

  divider: {
    padding: 10,
    borderRightWidth: 0.5,
    borderRightColor: theme.COLORS.MUTED,
  },

  search: {
    height: 35,
    width: 250,
    marginRight: 200,
    marginHorizontal: 16,
    borderColor: '#010101',
    borderWidth: 2,
    borderRadius: 30,
  },

  tabs: {
    marginBottom: 10,
    marginTop: 10,
    elevation: 4,
  },

  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.3,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  
  tabTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 5,
    paddingRight: 50
  },

  categoryListContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
})