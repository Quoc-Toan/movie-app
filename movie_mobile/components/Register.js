import React, { Component } from 'react';
import { Text, TextInput, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImagePhone from '../assets/usser.png';
import Key from '../assets/pass.png';
import Email from '../assets/email.png';
export default function Register(props) {
    return <View >
    <Text style={styles.title}>SMART <Text style={styles.title1}>APP</Text></Text>
        <View style={styles.View2}>
            <Image style={styles.userImg} source={ImagePhone} />
            <TextInput style={styles.inputUsername} placeholder="User name" ></TextInput>
            <Image style={styles.userImg1} source={Key} />
            <TextInput style={styles.inputUsername1} secureTextEntry placeholder="Password" ></TextInput>
            <Image style={styles.userImg1} source={Key} />
            <TextInput style={styles.inputUsername1} secureTextEntry placeholder="Re-enter password" ></TextInput>
            <Image style={styles.userImg1} source={Email} />
            <TextInput style={styles.inputUsername1} placeholder="Email" ></TextInput>
        </View>
       <Text style={styles.submitText} >Sign Up</Text>
        <Text style={styles.askAccount} >Already have an account ?<Text style={styles.askAccount1}> Login</Text> </Text>
    </View>
}


const styles = StyleSheet.create(
    {
        View2: {
            marginTop: 10,
            position: 'relative',
            top: 80,
        },
        askAccount:
        {
            position : 'relative',
            bottom :-70,
            marginLeft: 38,
            fontWeight: 'bold',
        },
        askAccount1:
        {

            margin: 10,
            marginLeft: 40,
            color: 'blue',

        },

        submitText: {


            width: 180,
            height: 70,
            paddingTop: 20,
            paddingBottom: 20,
            color: '#fff',
            textAlign: 'center',
            backgroundColor: '#08338A',
            borderRadius: 50,
            borderWidth: 1,
            borderColor: '#fff',
            marginTop: 80,
            fontSize: 17,
            fontWeight: 'bold',
            position: 'relative',
            bottom: -35,
            right: -50,
        },
        userImg: {
            width: 20,
            height: 20,
            position: 'relative',
            right: -8,
            bottom: -38
        },
        userImg1: {
            width: 20,
            height: 20,
            position: 'relative',
            right: -8,
            bottom: -38
        },
        inputUsername:
        {
            paddingHorizontal: 40,
            borderRadius: hp('20'),
            borderColor: ('#000'),
            borderStyle: 'solid',
            borderWidth: hp('0.15'),
            height: hp('7'),
            width: wp('70'),

        },

        inputUsername1:
        {

            paddingHorizontal: 40,
            borderRadius: hp('20'),
            borderColor: ('#000'),
            borderStyle: 'solid',
            borderWidth: hp('0.15'),
            height: hp('7'),
            width: wp('70'),

        },
        title: {
            fontSize: 35,
            textAlign: 'center',
            paddingBottom: 20,
            fontWeight: 'bold'

        },

        title1: {
            fontSize: 35,
            textAlign: 'center',
            color: '#00C2FF',
            paddingBottom: 20,
            fontWeight: 'bold'
        },
    }

);