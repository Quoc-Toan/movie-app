import React, {useState} from 'react';
import { Button, View, Text, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";
export default class InputNewPassword extends React.Component {
 
    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView
                keyboardVerticalOffset={-40}
                behavior={'padding'}
                style={styles.containerAvoidingView}
                >
                    <Text style={styles.textTitle}>{"Please input your new password"}</Text>
                    <View style={styles.containerInput}> 
                        <TextInput
                        style={styles.inputEmail}
                        placeholder="new password"
                        secureTextEntry={false}
                        />
                    </View>
                    <View style={styles.containerInput}> 
                        <TextInput
                        style={styles.inputEmail}
                        placeholder="confirm new password"
                        secureTextEntry={false}
                        />
                    </View>
                    <View style={styles.viewBottom}>
                        <TouchableOpacity onPress={() => navigation.navigate('Sign In')}>
                            <View style={styles.btnContinue}>
                                <Text style={{color: '#fff', alignItems: 'center'}}>Continue</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerAvoidingView: {
        flex: 1,
        alignItems: 'center',
        padding: 10
    },
    textTitle: {
        marginBottom: 50,
        marginTop: 50,
        fontSize: 15
    },
    containerInput: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        borderRadius: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
        borderBottomColor: 1.5,
        marginBottom: 30
        
    },
    inputEmail: {
        paddingHorizontal: 40,
        borderRadius: hp("20"),
        borderColor: "#000",
        borderStyle: "solid",
        height: hp("7"),
        width: wp("70"),
    },
    viewBottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 50,
        alignItems: 'center'
    },
    btnContinue: {
        width: 150,
        height: 50,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#d72631'
    }
  });
  