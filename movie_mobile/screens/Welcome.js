import React from 'react';
import { StyleSheet, StatusBar, Dimensions, TouchableOpacity, Text, View, Image } from 'react-native';
import { theme, Block, Button } from 'galio-framework';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { LinearGradient } from 'expo-linear-gradient';
const { height, width } = Dimensions.get('screen');
import Images from '../constants/Images';

export default class Onboarding extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <Block flex style={styles.container}>
        <StatusBar barStyle="light-content" />
        <Block flex center>
          <Image style={styles.homeImage} source={Images.Logo} />
        </Block>
        <Block flex space="between" style={styles.padded}>
          <Block flex space="around" style={{ zIndex: 2 }}>
            <Block center>
              <Button
                shadowless
                style={styles.button}
                textStyle={styles.optionsText}
                onPress={() => navigation.navigate('App')}>
                 GET STARTED
              </Button>
            </Block>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create(
  {
    container: {
      alignItems: 'center'
    },
    homeImage:
    {
      // marginTop: 120,
      width: width/ 4 + width / 2,
      height: width/ 2 + width / 4,
      alignSelf: 'center',
      top: "35%",
      position: 'relative',
      bottom: -100,
    },

    padded: {
      paddingHorizontal: theme.SIZES.BASE * 2,
      position: 'relative',
      bottom: theme.SIZES.BASE,
    },
    button: {
      width: width - width / 2,
      height: height / 16,
      backgroundColor: '#d72631',
      shadowColor: 'rgba(0, 0, 0, 0)',
      elevation: 10,
      shadowRadius: 10 ,
      shadowOffset : { width: 14, height: 20},
    },
    optionsText: {
      fontSize: theme.SIZES.BASE * 1.2 ,
      color: '#fff',
      fontWeight: "normal",
      fontStyle: "normal",    
    },
  });