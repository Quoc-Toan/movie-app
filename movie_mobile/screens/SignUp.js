import React from "react";
import {
  ImageBackground,
  Image,
  StyleSheet,
  StatusBar,
  Dimensions,
  Platform,
  View,
  TextInput,
  Alert,
  ScrollView,
} from "react-native";
import { Block, Button, Text, theme } from "galio-framework";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { height, width } = Dimensions.get("screen");
import ImagePhone from "../assets/images/usser.png";
import Key from "../assets/images/pass.png";
import Email from "../assets/images/email.png";
import ImageLogo from "../assets/images/Logo1.jpg";
import md5 from 'md5';

const HOST = "https://66ca6d6ab1bb.ngrok.io";

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);

    // tạo biến
    this.state = {
      fullName: "",
      password: "",
      rePassword: "",
      email: "",
      isValidEmail: true,
      isValidPassword: true,
      isValidFullName: true,
      isValidrePasss: true,
    };
    this.signUp = this.signUp.bind(this);
  }

  signUp = async () => {
    const result = await this.validate();

  //  if (result) 
      const res = await fetch(`${HOST}/Account/Account-Exists`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          idAccount: 0,
          username: this.state.fullName,
          password: md5(this.state.password),
          fullName: "",
          email: this.state.email,
          phone: 0,
          role: 1,
          status: true,
          urlAvatar: "",
        }),
      });
      const json = await res.json();

      if (json) {
        Alert.alert("Username Already exists!");
      } else {
        const res = await fetch(`${HOST}/Account/Account-appr-create`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            idAccount: 0,
            username: this.state.fullName,
            password: md5(this.state.password),
            fullName: "",
            email: this.state.email,
            phone: 0,
            role: 1,
            status: true,
            urlAvatar: "",
          }),
        });
        const json2 = await res.json();
        if (json2 != null) {
          Alert.alert("Sign up success");
          this.props.navigation.navigate("Sign In");
        }
      }
   // }
  };

  onfullName = (fullName) => {
    this.setState({
      ...this.state, // tránh thay đổi các thẻ text khác.
      fullName: fullName,
    });
  };
  onpassword = (password) => {
    this.setState({
      ...this.state, // tránh thay đổi các thẻ text khác.
      password: password,
    });
  };
  onrepassword = (rePassword) => {
    this.setState({
      ...this.state, // tránh thay đổi các thẻ text khác.
      rePassword: rePassword,
    });
  };

  onEmail = (email) => {
    this.setState({
      ...this.state, // tránh thay đổi các thẻ text khác.
      email: email,
    });
  };

  validateEmail = (email) => {
    const expression =
      /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    const result = expression.test(String(email).toLowerCase());

    this.setState({
      ...this.state,
      isValidEmail: result,
    });

    return result;
  };
  // valid password
  validatePassword = (password) => {
    const result = password.length >= 8 && password.length <= 20;

    this.setState({
      ...this.state,
      isValidPassword: result,
    });

    return result;
  };

  validFullName = (fullName) => {
    const result = fullName != "";

    this.setState({
      ...this.state,
      isValidFullName: result,
    });

    return result;
  };

  validrePassword = (rePassword, password) => {
    const result = true;

    this.setState({
      ...this.state,
      isValidrePasss: result,
    });

    return result;
  };

  validate = async () => {
    var result = true;
    const { email, password, fullName } = this.state;
    result = (await this.validateEmail(email)) && result;
    result = (await this.validatePassword(password)) && result;
    result = (await this.validFullName(fullName)) && result;
    result = (await this.validrePassword(fullName)) && result;
    console.log(result);

    return result;
  };

  //Input
  renderInput = () => {
    const {
      fullName,
      password,
      rePassword,
      email,
      isValidEmail,
      isValidPassword,
      isValidFullName,
      isValidrePasss,
    } = this.state;
    return (
      <View style={styles.ViewInput}>
        <Image style={styles.userImg} source={ImagePhone} />
        <TextInput
          style={styles.inputUsername}
          placeholder="Full Name"
          value={fullName}
          onChangeText={(text) => this.onfullName(text)}
        ></TextInput>
        {!isValidFullName ? (
          <Text muted color="red">
            Full name is empty!
          </Text>
        ) : (
          <Text></Text>
        )}

        <Image style={styles.Key1} source={Key} />
        <TextInput
          style={styles.inputUsername1}
          secureTextEntry
          placeholder="Password"
          value={password}
          onChangeText={(text) => this.onpassword(text)}
        ></TextInput>
        {!isValidPassword ? (
          <Text style={styles.errRe} muted color="red">
            Pass is invalid!
          </Text>
        ) : (
          <Text></Text>
        )}

        <Image style={styles.keyMatch} source={Key} />
        <TextInput
          style={styles.inputUsernameRepass}
          secureTextEntry
          placeholder="Re-enter password"
          value={rePassword}
          onChangeText={(text) => this.onrepassword(text)}
        ></TextInput>
        {!isValidrePasss ? (
          <Text muted color="red" style={styles.errPass}>
            Password is not match!
          </Text>
        ) : (
          <Text></Text>
        )}

        <Image style={styles.reKey} source={Email} />
        <TextInput
          style={styles.inputEmail}
          placeholder="Email"
          value={email}
          onChangeText={(text) => this.onEmail(text)}
        ></TextInput>
        {!isValidEmail ? (
          <Text muted color="red" style={styles.errEmail}>
            Email is invalid!
          </Text>
        ) : (
          <Text></Text>
        )}
      </View>
    );
  };

  //Have Account
  renderHaveAccount = () => {
    const { navigation } = this.props;
    return (
      <Text center style={{ marginTop: 5 }}>
        <Text style={styles.askAccount}>
          Already have an account ?
          <Text
            style={styles.askAccount1}
            onPress={() => navigation.navigate("Sign In")}
          >
            {" "}
            Sign In
          </Text>
        </Text>
      </Text>
    );
  };

  //Button sign in
  renderButtonSignUp = () => {
    return (
      <Block center>
        <Button
          shadowless
          color="#000000"
          style={[styles.submitText, styles.shadow]}
          onPress={this.signUp}
        >
          SIGN UP
        </Button>
      </Block>
    );
  };

  //Display
  render() {
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: "#ffff", width: width }}
        showsVerticalScrollIndicator={false}
      >
        <ImageBackground
          style={{ height: Dimensions.get("window").height / 3.5 }}
          source={ImageLogo}
        ></ImageBackground>
        {this.renderInput()}
        {this.renderButtonSignUp()}
        {this.renderHaveAccount()}
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  ViewInput: {
    marginTop: 30,
    marginLeft: 55,
    position: "relative",
  },

  askAccount: {
    position: "relative",
    bottom: -120,
    fontWeight: "bold",
  },

  askAccount1: {
    fontSize: 20,
    margin: 10,
    marginLeft: 40,
    color: "#d72631",
    fontStyle: "italic",
  },

  submitText: {
    marginTop: -20,
    borderRadius: hp("20"),
    height: hp("7"),
    width: wp("50"),
  },

  userImg: {
    width: 20,
    height: 20,
    position: "relative",
    right: -8,
    bottom: -38,
  },
  userImg1: {
    width: 20,
    height: 20,
    position: "relative",
    right: -8,
    bottom: -38,
  },
  inputUsername: {
    paddingHorizontal: 40,
    borderRadius: hp("20"),
    borderColor: "#000",
    borderStyle: "solid",
    borderWidth: hp("0.15"),
    height: hp("7"),
    width: wp("70"),
  },

  inputUsernameRepass: {
    position: "relative",
    top: -40,
    paddingHorizontal: 40,
    borderRadius: hp("20"),
    borderColor: "#000",
    borderStyle: "solid",
    borderWidth: hp("0.15"),
    height: hp("7"),
    width: wp("70"),
  },

  inputUsername1: {
    position: "relative",
    top: -20,
    paddingHorizontal: 40,
    borderRadius: hp("20"),
    borderColor: "#000",
    borderStyle: "solid",
    borderWidth: hp("0.15"),
    height: hp("7"),
    width: wp("70"),
  },
  title: {
    fontSize: 35,
    textAlign: "center",
    paddingBottom: 20,
    fontWeight: "bold",
    position: "relative",
    bottom: -90,
  },

  title1: {
    fontSize: 35,
    textAlign: "center",
    color: "#00C2FF",
    paddingBottom: 20,
    fontWeight: "bold",
  },

  errRe: {
    position: "relative",
    top: -20,
  },
  inputEmail: {
    position: "relative",
    top: -60,
    paddingHorizontal: 40,
    borderRadius: hp("20"),
    borderColor: "#000",
    borderStyle: "solid",
    borderWidth: hp("0.15"),
    height: hp("7"),
    width: wp("70"),
  },
  errPass: {
    position: "relative",
    top: -40,
  },
  errEmail: {
    position: "relative",
    top: -60,
  },
  Key1: {
    position: "relative",
    top: 20,
    width: 20,
    height: 20,
    right: -13,
  },
  reKey: {
    position: "relative",
    top: -20,
    width: 20,
    height: 20,
    right: -13,
  },
  keyMatch: {
    position: "relative",
    top: 1,
    width: 20,
    height: 20,
    right: -13,
  },
});
