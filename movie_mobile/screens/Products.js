import React from 'react';
import { StyleSheet, Dimensions, FlatList, View, Image, TouchableOpacity} from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Images, materialTheme, products } from '../constants';
import Icon from 'react-native-vector-icons/MaterialIcons';

const host = 'https://f89ea1e8f415.ngrok.io';
const { width } = Dimensions.get('screen');
const cardWidth = (width - 20 - 20) / 2;

export default class Products extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
      isLoading: true,
    }
  }

  componentDidMount() {
    fetch (`${host}/Film/Film-appr-list`)
      .then ( (response) => response.json() )
      .then ( (responeJson) => {

        this.setState({
          isLoading: false,
          data: responeJson
        })
      })
  .catch((error) => {
    console.log(error)
  });
  }

  renderSearch = () => {
    const { navigation } = this.props;
    const iconCamera = <Icon size={16} color={theme.COLORS.MUTED} name="zoom-in" family="material" />

    return (
      <Input
        style={styles.search}
        iconContent={iconCamera}
        placeholder="What are you looking for?"
        onFocus={() => navigation.navigate('Movie')}
      />
    )
  }
  
  
  renderMovieCard = (movie) => {
    const { navigation } = this.props;
      return (
        <TouchableOpacity
        activeOpacity={1}
        onPress={() => navigation.navigate('Movie Detail', movie)}>
          <View style={{...styles.card}}>
            <View style={{...styles.cardOverLay}}>
              <View
                style={{
                  position: 'absolute',
                  top: 5,
                  right: 5,
                  zIndex: 1,
                  flexDirection: 'row',
                }}>
                <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
                <Text style={{color: materialTheme.COLORS.WHITE, fontWeight: 'bold', fontSize: 15}}>
                  {movie.star}
                </Text>
              </View>
              <Image source={{uri: `${host}/Data/Images/` + movie.urlPoster}} style={styles.cardImage}/>
              <View style={{paddingVertical: 5, paddingHorizontal: 10}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>{movie.filmName}</Text>

            </View>
            </View>
          </View>
        </TouchableOpacity>
      );

    // return (
    //   <Block flex style={styles.group}>
    //       <Block>
    //         <FlatList
    //             data={movies}
    //             showsVerticalScrollIndicator={false}
    //             contentContainerStyle={{
    //               marginVertical: 5,
    //               marginHorizontal: 15
    //             }}
    //             numColumns={2}
    //             renderItem={({item}) => <Card movie={item} />}
    //           />
    //       </Block>
    //   </Block>
    // )
  }

  render() {
    const {data} = this.state
    return (
      <Block row space="between" style={{ marginTop: theme.SIZES.BASE, flexWrap: 'wrap' }} >
          {data.map(film => this.renderMovieCard(film))}
      </Block>
    );
  }
}

const styles = StyleSheet.create({

  components: {
    width: width
  },

  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },

  group: {
    paddingTop: theme.SIZES.BASE * -1,
  },

  card: {
    height: 260,
    elevation: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    marginRight: 10,
    marginBottom: 20,
    width: cardWidth,
  },

   cardImage: {
    height: 210,
    width: '100%',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },

  cardOverLay: {
    height: 260,
    backgroundColor: materialTheme.COLORS.WHITE,
    position: 'absolute',
    zIndex: 100,
    width: cardWidth,
    borderRadius: 15,
  },

  cardDetails: {
    height: 100,
    borderRadius: 15,
    backgroundColor: materialTheme.COLORS.WHITE,
    position: 'absolute',
    bottom: 0,
    padding: 20,
    width: '100%',
  },
});