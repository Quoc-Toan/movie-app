import React from "react";
import {
  ImageBackground,
  Image,
  StyleSheet,
  StatusBar,
  ScrollView,
  Dimensions,
  Platform,
  TouchableOpacity,
  View,
} from "react-native";
import { Block, Button, Text, theme } from "galio-framework";
import { LinearGradient } from "expo-linear-gradient";
const { height, width } = Dimensions.get("screen");
import { Images, materialTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import { SafeAreaView } from "react-native-safe-area-context";
import Icon from 'react-native-vector-icons/MaterialIcons';

const host = 'https://f89ea1e8f415.ngrok.io';

export default class MovieDetail extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      productdetail: {},
    };
  }

  componentDidMount() {
    this.setState({
      productdetail: this.props.route.params
    })
  }


  renderProductDetails = (productdetail) => {
    return(
      <Block flex>
          <ImageBackground
            style={{flex: 0.7}}
            source={{uri: `${host}/Data/Images/` + productdetail.urlPoster}}
            style={{ height: height / 2.5, width: width }}
          >
          </ImageBackground>     
          <Block>
            <Text style={styles.nameproduct}>{productdetail.filmName}</Text>
          </Block>
          <Block>
            <Text style={styles.priceproduct} size={16} color={materialTheme.COLORS.WARNING}>
              {productdetail.rate} <Icon name="star" family="GalioExtra" size={14} />
            </Text>
          </Block>
          <Block>
            <Text style={styles.description}>Detailed Description: </Text>
            <Text style={styles.detail}>{productdetail.detailFilm}</Text>
          </Block>
      </Block>
    )
  }

  renderButton = () =>{
    const { navigation } = this.props;
    return(
      <Block flex style={styles.padded}>
        <TouchableOpacity style={styles.button}>
        <Button
          shadowless
          style={styles.button}
          color={materialTheme.COLORS.BUTTON_COLOR}
          onPress={() => navigation.navigate('Cart')}>
          Watch
        </Button>
        </TouchableOpacity>
      </Block>
    )
  }

  render() {
    const { productdetail } = this.state;

    return (
      <SafeAreaView>
        {productdetail != null ? (
          <Block flex style={styles.container}>
            {this.renderProductDetails(productdetail)}
            {this.renderButton()}
          </Block>
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },
  nameproduct: {
    fontSize: 25,
    paddingTop: 15,
    paddingLeft: 10,
    fontWeight: "bold",
  },
  priceproduct: {
    fontSize: 25,
    paddingTop: 50,
    paddingLeft: 10,
    fontWeight: "bold",
    color: "#ff1a1a",
  },
  description: {
    fontSize: 15,
    paddingTop: 100,
    paddingLeft: 10,
    fontWeight: "bold",
  },
  detail: {
    marginTop: 10,
    paddingLeft: 10,
    fontSize: 13,
    padding: 10,
  },
  padded: {
    position: "absolute",
    bottom:
      Platform.OS === "android" ? theme.SIZES.BASE * -45 : theme.SIZES.BASE * 3,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
    bottom: -720,
  },
  button: {
    backgroundColor: 'rgb(255, 57, 69)',
    width: 340,
    flexDirection: "row",
    height: 50,
    borderWidth: 0,
    alignItems: "center",
    fontSize: 25
  },
  imageStyle: {
    padding: 10,
    margin: 5,
    height: 30,
    width: 30,
    resizeMode: "stretch",
  },
  add: {
    flexDirection: "row",
    paddingLeft: 30,
    justifyContent: "center",
    paddingTop: 5,
  },
  quanlity: {
    width: 30,
    textAlign: "center",
    fontSize: 25,
    alignContent: "center",
  },
  image: {
    height: 25,
    width: 25,
    padding: 10,
    margin: 5,
    resizeMode: "stretch",
  },
  header: {
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    justifyContent: 'space-between',
  },
});
