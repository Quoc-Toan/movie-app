import React, { useState, useRef } from 'react';
import { Button, View, Text, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import OTPTextInput from 'react-native-otp-textinput'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
export default class InputOTP extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            otpText: ""
        }
    }

    onChangeText = (val) => {
        this.setState({
            ...this.state,
            otpText: val
        })
    };

    comfirmOPT() {
        console.log(this.state.otpText)

        this.props.navigation.navigate('Input New Password')
    }

    render() {
        const lengthInput = 6;
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView
                    keyboardVerticalOffset={-40}
                    behavior={'padding'}
                    style={styles.containerAvoidingView}
                >
                    <Text style={styles.textTitle}>{"Input your OTP code sent email"}</Text>
                    <View>
                        <OTPTextInput
                            inputCount={lengthInput}
                            handleTextChange={text => this.onChangeText(text)}
                             />
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.viewBottom}>
                    <TouchableOpacity onPress={() => this.comfirmOPT()}>
                        <View style={styles.btnContinue}>
                            <Text style={{ color: '#fff', alignItems: 'center' }}>Continue</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerAvoidingView: {
        flex: 1,
        alignItems: 'center',
        padding: 10
    },
    textTitle: {
        marginBottom: 50,
        marginTop: 50,
        fontSize: 15
    },
    containerInput: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cellView: {
        paddingVertical: 11,
        width: 40,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1.5
    },
    cellText: {
        textAlign: 'center',
        fontSize: 16
    },
    btnContinue: {
        width: 150,
        height: 50,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#d72631'
    }
});
