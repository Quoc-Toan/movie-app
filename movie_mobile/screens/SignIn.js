import React from 'react';
import { Image, StyleSheet, Dimensions, Platform, View, TextInput, Alert, ScrollView, ImageBackground} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const { height, width } = Dimensions.get('screen');
import ImagePhone from '../assets/images/usser.png';
import Key from '../assets/images/pass.png';
import ImageLogo from '../assets/images/Logo1.jpg';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import md5 from 'md5';

const HOST = "https://f89ea1e8f415.ngrok.io";


export default class SignIn extends React.Component {

  constructor(props) {
    super(props)
    
    // tạo biến
    this.state = {
      username: '',
      password: ''
    }

    this.signIn = this.signIn.bind(this)
  }

    //logic đăng nhập
    signIn = async () => {
      try {
        const res = await fetch(`${HOST}/login`, { 
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: this.state.username,

            password: md5(this.state.password)
          })
        })
        const json = await res.json();
  
        if(json) {
          Alert.alert("Đăng nhập thành công!")
          this.props.navigation.navigate("Home")
        } else {
          Alert.alert("Đăng nhập thất bại!")
        }
      } catch (error) {
        console.error(error);
      }
    }
 
  // hàm để change value , nói chung input nó nhận
  onUsernaneChange = (username) => {
    this.setState({
      ...this.state, // tránh thay đổi các thẻ text khác.
      username: username
    })
  }

    // hàm để change value , nói chung input nó nhận
    onPasswordChange = (password) => {
      this.setState({
        ...this.state, // tránh thay đổi các thẻ text khác.
        password: password
      })
    }

    //Ask have account
    renderAskAccount = () => {
      const { navigation } = this.props;
      return(
        <View style={styles.bottomView}>
          <View style={{padding: 20}}> 
            <Text style={{color: '#ffffff', fontSize: 40, fontStyle: 'italic'}}>Welcome</Text>
            <Text>
              <Text style={styles.askAccount} >Don't have an account?
                <Text style={styles.askSignUp} onPress={() => navigation.navigate('Sign Up')}>  Sign Up</Text> 
              </Text>
            </Text>
          </View>
        </View>
      )
    }

    // Input
    renderInput = () => {    
    // gọi lại biến state
    const { username, password } = this.state;
      return(
        <View style={styles.ViewInput}>
          <Image style={styles.userImg} source={ImagePhone} />   
          <TextInput value={username} style={styles.inputUsername} onChangeText = {text => this.onUsernaneChange(text)} placeholder="Usernane" ></TextInput>
          <Image style={styles.userImg1} source={Key} />
          <TextInput value={password} style={styles.inputPassword} secureTextEntry placeholder="Password" onChangeText = {text => this.onPasswordChange(text)} ></TextInput>
        </View>
      )
    }

    //Button sign in
    renderButtonSignIn = () => {
      return(
        <Block center>
          <Button shadowless color="#d72631" style={[styles.submitText, styles.shadow]}
            onPress={this.signIn}
          >
            SIGN IN
          </Button>
        </Block>
      )
    }

  //Forgot password
  renderForgot = () => {
    const { navigation } = this.props;
    return (
      <Block center style={{ marginTop: 5 }}>
        <Text style={styles.forgotPass}
        onPress={() => navigation.navigate("Authentication")}
        > Forgot Password?
        </Text>
      </Block>
    );
  };

    //social
    renderSocial = () => {
      return (
        <Block flex style={styles.group}>
          <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
            <Block row center space="between">
              <Block flex middle right>
                <Button
                  round
                  onlyIcon
                  shadowless
                  icon="facebook"
                  iconFamily="font-awesome"
                  iconColor={theme.COLORS.WHITE}
                  iconSize={theme.SIZES.BASE * 1}
                  color={theme.COLORS.FACEBOOK}
                  style={[styles.social, styles.shadow]}
                />
              </Block>
              <Block flex middle center>
                <Button
                  round
                  onlyIcon
                  shadowless
                  icon="twitter"
                  iconFamily="font-awesome"
                  iconColor={theme.COLORS.WHITE}
                  iconSize={theme.SIZES.BASE * 1}
                  color={theme.COLORS.TWITTER}
                  style={[styles.social, styles.shadow]}
                />
              </Block>
              <Block flex middle left>
                <Button
                  round
                  onlyIcon
                  shadowless
                  icon="google"
                  iconFamily="font-awesome"
                  iconColor={theme.COLORS.WHITE}
                  iconSize={theme.SIZES.BASE * 1}
                  color={'#d72631'}
                  style={[styles.social, styles.shadow]}
                />
              </Block>
            </Block>
          </Block>
        </Block>
      )
    }

  render() {
    return (
       <ScrollView
        style={{flex: 1, backgroundColor: '#ffff',width: width}} showsVerticalScrollIndicator={false}>
          <ImageBackground style={{height: Dimensions.get('window').height / 3.5,}} source={ImageLogo}></ImageBackground>
            {this.renderAskAccount()}
            {this.renderInput()}
            {this.renderButtonSignIn()}
            {this.renderForgot()}
            {this.renderSocial()}
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create(
  {
    brandView:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },

    bottomView:{
      flex: 1.5,
      backgroundColor: '#010101',
      borderTopStartRadius: 60,
      borderTopEndRadius: 60
    },  

    ViewInput: {
      marginTop: 30,
      marginLeft: 55,
      position: 'relative',
    },

    askAccount:
    {
      color: '#fff',
      position: 'relative',
      bottom: -120,
      fontWeight: 'bold',
    },

    askSignUp:
    {
      fontSize: 20,
      margin: 10,
      marginLeft: 40,
      color: '#d72631',
      fontStyle: 'italic'
    },

    submitText: {
      marginTop: 50,
      borderRadius: hp('20'),
      height: hp('7'),
      width: wp('50'),
    },

    userImg: {
      width: 20,
      height: 20,
      position: 'relative',
      right: -8,
      bottom: -38
    },

    userImg1: {
      width: 20,
      height: 20,
      position: 'relative',
      right: -8,
      bottom: -38
    },

    inputUsername:
    {
      paddingHorizontal: 40,
      borderRadius: hp('20'),
      borderColor: ('#000'),
      borderStyle: 'solid',
      borderWidth: hp('0.15'),
      height: hp('7'),
      width: wp('70'),
    },

    inputPassword:
    {
      paddingHorizontal: 40,
      borderRadius: hp('20'),
      borderColor: ('#000'),
      borderStyle: 'solid',
      borderWidth: hp('0.15'),
      height: hp('7'),
      width: wp('70'),
    },

    forgotPass: {
      margin: 20,
      fontStyle: 'italic'
    },

    group: {
      paddingTop: theme.SIZES.BASE * -10,
    },
  }
);
