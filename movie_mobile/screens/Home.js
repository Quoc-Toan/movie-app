import React,{ useEffect, useState }from 'react';
import { Button, Block, Text, Input, theme, Card } from 'galio-framework';
import { Images, materialTheme, products } from '../constants';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, ScrollView, ActivityIndicator, ImageBackground, TouchableWithoutFeedback, FlatList, Animated} from 'react-native';
import movies from '../constants/movies'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { isLoaded } from 'expo-font';


const { width,height } = Dimensions.get('screen');
const cardWidth = width / 1.8;
const thumbMeasure = (width - 48 - 25) / 2;

const host = 'https://f89ea1e8f415.ngrok.io';

export default class Home extends React.Component {
  
constructor(props){
  super(props);
  this.state = {
    isLoading: true,
    data: [{
      "idFilm": 0,
    "filmName": '',
    "idProducer": 0,
    "idCategory": 0,
    "idCountry": 0,
    "view": 0,
    "rate": 0,
    "releaseDate": '',
    "limitAge": 0,
    "status": true,
    "urlFilm": '',
    "detailFilm": '',
    "urlPoster": ''
    }]
  }

}

componentDidMount(){
  this.loadList();
}


  loadList(){
  return fetch (`${host}/Film/Film-appr-list`)
      .then ( (response) => response.json() )
      .then ( (responeJson) => {

        this.setState({
          isLoading: false,
          data: responeJson
        })
      })
  .catch((error) => {
    console.log(error)
  });
}

  renderTop () {
//this.loadList();
    const { navigation } = this.props;
    const scrollX = new Animated.Value(0);

  if(this.state.isLoading){
      return (
        <View style={styles.container}>
          <Text>Loading</Text>
        </View>
      )


  } else {

    const Card = ({data, index}) => {

        const inputRange = [
          (index - 1) * cardWidth,
          index * cardWidth,
          (index + 1) * cardWidth,
        ];
        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [0.7, 0, 0.7],
        });
        const scale = scrollX.interpolate({
          inputRange,
          outputRange: [0.8, 1, 0.8],
        });

        return (
        <TouchableOpacity
        activeOpacity={1}
        onPress={() => navigation.navigate('Movie Detail', data)}>
          <Animated.View style={{...styles.card, transform: [{scale}]}}>
              <Animated.View style={{...styles.cardOverLay, opacity}} />
              <View style={styles.priceTag}>
                <Text style={{color: materialTheme.COLORS.WHITE, fontSize: 20, fontWeight: 'bold', fontStyle: 'italic'}}>New</Text>
              </View>
              <Image source={{uri: `${host}/Data/Images/` + data.urlPoster}} style={styles.cardImage}/>
              <View style={styles.cardDetails}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View>
                    <Text style={{fontWeight: 'bold', fontSize: 17}}>
                      { data.filmName}
                    </Text>
                    <Text style={{color: materialTheme.COLORS.PRIMARY, fontSize: 12}}>
                      {/* {movie.idFilm} */}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 10,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
                    <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
                    <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
                    <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
                  </View>
                  <Text style={{fontSize: 10, color: materialTheme.COLORS.BLACK}}>365reviews</Text>
                </View>
              </View>
            </Animated.View>  
          </TouchableOpacity>
        );
      };
    
    return(
      <Block flex style={styles.groupTop}>
        <Block flex>
          <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
            <Animated.FlatList
               onScroll={Animated.event(
                [{nativeEvent: {contentOffset: {x: scrollX}}}],
                {useNativeDriver: true},
              )}
              horizontal
              data={this.state.data}
              contentContainerStyle={{
                paddingVertical: 30,
                paddingLeft: 20,
                paddingRight: cardWidth / 2 - 40,
              }}
              showsHorizontalScrollIndicator={false}
              renderItem={({item, index}) => <Card data={item} index={index}/>}
              snapToInterval={cardWidth}
           
            />
            
          </Block>
        </Block>
      </Block>
    )
            }
  }

  renderAction = () => {
    const { navigation } = this.props;

    const TopTrendingCard = ({movie}) => {
      return(
        <View style={styles.topTrendingCard}>
          <View
            style={{
              position: 'absolute',
              top: 5,
              right: 5,
              zIndex: 1,
              flexDirection: 'row',
            }}>
            <Icon name="star" size={15} color={materialTheme.COLORS.WARNING} />
            <Text style={{color: materialTheme.COLORS.WHITE, fontWeight: 'bold', fontSize: 15}}>
              {movie.star}
            </Text>
          </View>
          <Image source={movie.image} style={styles.topTrendingCardImage}/>
          <View style={{paddingVertical: 5, paddingHorizontal: 10}}>
            <Text style={{fontSize: 10, fontWeight: 'bold'}}>{movie.name}</Text>
            <Text style={{fontSize: 7, fontWeight: 'bold', color: materialTheme.COLORS.MUTED}}>
              {movie.location}
            </Text>
          </View>
        </View>
      );
    };

    return (
      <Block flex style={[styles.group]}>
        <Text bold size={16} style={styles.title}>Top Trending</Text>
        <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
          <Block flex right>
            <Text
              size={14}
              color={theme.COLORS.PRIMARY}
              onPress={() => navigation.navigate('Movie')}>
              See All
            </Text>
          </Block>
          <FlatList
            data={movies}
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              paddingLeft: 20,
              marginTop: 20,
              paddingBottom: 30,
            }}
            renderItem={({item}) => <TopTrendingCard movie={item} />}
          />
        </Block>
      </Block>
    )
  }

  render() {
    return (
      <Block flex center>
        <ScrollView
          style={styles.components}
          showsVerticalScrollIndicator={false}>
            {this.renderTop()}
            {this.renderAction()}
            {this.renderAction()}
        </ScrollView>
      </Block>
    )
  }
}

const styles = StyleSheet.create({

  container:{
flex: 1,
backgroundColor: '#fff',
alignItems: 'center',
justifyContent: 'center'
  },

  groupTop:{
    paddingTop: theme.SIZES.BASE * -1,
  },

  group: {
    paddingTop: theme.SIZES.BASE * -1,
  },

  title: {
    color: '#010101',
    paddingVertical: theme.SIZES.BASE,
    paddingHorizontal: theme.SIZES.BASE * 2,
  },
  
  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.2,
    elevation: 2,
  },

  albumThumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },

  imageBlock: {
    overflow: 'hidden',
    borderRadius: 4,
  },

  categoryTitle: {
    height: '100%',
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },

  card: {
    height: 280,
    elevation: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    marginRight: 20,
    width: cardWidth,
    borderWidth: 0,
  },

  cardImage: {
    height: 200,
    width: '100%',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },

  priceTag: {
    height: 50,
    width: 80,
    backgroundColor: '#d72631',
    position: 'absolute',
    zIndex: 1,
    right: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
  cardDetails: {
    height: 100,
    borderRadius: 15,
    backgroundColor: materialTheme.COLORS.WHITE,
    position: 'absolute',
    bottom: 0,
    padding: 20,
    width: '100%',
  },

  cardOverLay: {
    height: 280,
    backgroundColor: materialTheme.COLORS.WHITE,
    position: 'absolute',
    zIndex: 100,
    width: cardWidth,
    borderRadius: 15,
  },

  topTrendingCard: {
    height: 220,
    width: 140,
    backgroundColor: materialTheme.COLORS.WHITE,
    elevation: 15,
    marginHorizontal: 10,
    borderRadius: 10,
  },

  topTrendingCardImage: {
    height: 180,
    width: '100%',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
});

 